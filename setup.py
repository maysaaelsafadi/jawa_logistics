# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in jawa_logistics/__init__.py
from jawa_logistics import __version__ as version

setup(
	name='jawa_logistics',
	version=version,
	description='Logistics App for Jawa',
	author='Jawa',
	author_email='doridelcahanap@gmail.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
