from __future__ import unicode_literals
from frappe import _

def get_data():
	return {
		'fieldname': 'file_info',
		'transactions': [
			{
				'label': _('Inventory'),
				'items': ['Delivery Note']
			},
			{
				'label': _('Selling'),
				'items': ['Sales Invoice']
			},
			{
				'label': _('Buying'),
				'items': ['Purchase Invoice']
			}
		]
	}