# -*- coding: utf-8 -*-
# Copyright (c) 2021, Jawa and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.model.mapper import get_mapped_doc
from frappe.model.utils import get_fetch_values
from frappe.contacts.doctype.address.address import get_company_address
from frappe.model.mapper import get_mapped_doc

class FileInfo(Document):

	def before_save(self):
		self.volume=''
		all_type=[]
		total_dict={}
		for row in self.container:
			if row.container_type not in all_type:
				all_type.append(row.container_type)
				total_dict[row.container_type]=1
			else:
				total_dict[row.container_type]=total_dict[row.container_type]+1

		volume=''
		for k, v in total_dict.items():
			volume='{0}\n {1}*{2}'.format(volume,v,k)
		self.volume='{} \n {}'.format(volume ,self.get("volume_details"))



	@frappe.whitelist()
	def create_delivery_note_for_container_info(self,container={}):
		try:
			way_bill = ""		
			if self.main_services == "Ocean":
				way_bill = self.bill_of_land

			if self.main_services == "Air":
				way_bill = self.air_waybill

			if self.main_services == "Land":
				way_bill = self.land_waybill

			if self.main_services == "House Bill":
				way_bill = self.house_bill

			# print(container.get("container_type"))
			# print("============================")
   
			#	frappe.msgprint(way_bill)
			new_note=frappe.new_doc("Delivery Note")
			new_note.file_info=self.name
			new_note.container_type=container.get("container_type")
			new_note.waybill = way_bill
			new_note.delivery_place = self.delivery_port
			new_note.main_service = self.main_services
			new_note.from_file=True
			new_note.customer=self.customer
			new_note.conversion_rate=1
			new_note.price_list_currency='SAR'
			new_note.plc_conversion_rate=1
			# for row in self.item:
			# 	new_note.append("file_info_item", {"description_of_goods":row.description_of_goods,
			# 					  "qty":row.qty,
			# 					  "unit":row.unit,
			# 					  "c_gross_weight":row.c_gross_weight,
			# 					  "c_volume":row.c_volume})
			# new_note.append("file_info_item", {
			# 	"description_of_goods":container.get("description_of_goods"),
			# 	"qty":container.get("qty"),
			# 	"unit":container.get("unit"),
			# 	"c_gross_weight":container.get("c_gross_weight"),
			# 	"c_volume":container.get("c_volume")}
			# )

			new_note.append("container_info", {"container_type":container.get("container_type"),
				"container_number":container.get("container_number"),
				"seal_number":container.get("seal_number"),
				"description":container.get("description")
    		})

			for row in self.item:
				new_note.append("file_info_item", {"description_of_goods":row.description_of_goods,
								"qty":row.qty,
								"unit":row.unit,
								"c_gross_weight":row.c_gross_weight,
								"c_volume":row.c_volume})
			print(new_note.file_info_item[0])
			new_note.save()

			if frappe.db.exists("Container Info",container.get("name")):
				frappe.db.set_value("Container Info",container.get("name"),"delivery_note",new_note.name)
				frappe.db.commit()

			# for row in self.container:
			# 	if row.container_type==container.get("container_type"):
			# 		row.delivery_note=new_note.name

			return new_note.name
		except Exception as error:
			traceback = frappe.get_traceback()
			frappe.log_error(message=traceback, title="Error While making Delivery Note.")
			frappe.throw("Something went wrong please try again.")



@frappe.whitelist()
def create_sales_invoice(source_name, target_doc=None):
	target_doc = get_mapped_doc("File Info", source_name,
				{"File Info": {
					"doctype": "Sales Invoice",
					"field_map": {
						"file_info": "name",
						"customer": "customer",
					}
				}}, target_doc)
	return target_doc

@frappe.whitelist()
def create_purchase_invoice(source_name, target_doc=None):
	target_doc = get_mapped_doc("File Info", source_name,
				{"File Info": {
					"doctype": "Purchase Invoice",
					"field_map": {
						"file_info": "name",
						"supplier": "supplier",
					}
				}}, target_doc)
	return target_doc

@frappe.whitelist()
def create_delivery_note(source_name, target_doc=None):
	way_bill = ""
	file_info_doc = frappe.get_doc('File Info', source_name)
	if file_info_doc.main_services == "Ocean":
		way_bill = file_info_doc.bill_of_land

	if file_info_doc.main_services == "Air":
		way_bill = file_info_doc.air_waybill

	if file_info_doc.main_services == "Land":
		way_bill = file_info_doc.land_waybill

	if file_info_doc.main_services == "House Bill":
		way_bill = file_info_doc.house_bill			

#	frappe.msgprint(way_bill)
	target_doc = get_mapped_doc("File Info", source_name,{
			"File Info": {
				"doctype": "Delivery Note",
				"field_map": {
					"file_info": "name"
				}
			},
			"File Info Item": {
				"doctype": "File Info Item",
			},
			"Container Info": {
				"doctype": "Container Info",
		}
	}, target_doc)
	target_doc.waybill = way_bill
	target_doc.delivery_place = file_info_doc.delivery_port
	target_doc.main_service = file_info_doc.main_services
	return target_doc


@frappe.whitelist()
def get_voucher(file_info):
	pinv = frappe.db.get_value('Purchase Invoice', {"docstatus": ["in", ['1','0']],"file_info": file_info}, ['name'])
	sinv = frappe.db.get_value('Sales Invoice', {"docstatus": ["in", ['1','0']],"file_info": file_info}, ['name'])
	dn = frappe.db.get_value('Delivery Note', {"docstatus": ["in", ['1','0']],"file_info": file_info}, ['name'])

	return pinv,sinv,dn
