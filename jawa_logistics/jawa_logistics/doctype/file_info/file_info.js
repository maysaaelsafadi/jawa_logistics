

// Copyright (c) 2021, Jawa and contributors
// For license information, please see license.txt

frappe.ui.form.on('File Info', {
  setup: function(frm) {
    frm.set_query("shipper", function () {
      return {
        filters: {
          type: 'Shipper'
        },
      };
    });
    frm.set_query("customer", function () {
      return {
        filters: {
          type: 'Customer'
        },
      };
    });
    frm.set_query("consignee", function () {
      return {
        filters: {
          type: 'Consignee'
        },
      };
    });
    frm.set_query("agent", function () {
      return {
        filters: {
          type: 'Agent'
        },
      };
    });
    frm.set_query("supplier", function () {
      return {
        filters: {
          type: 'Vendor'
        },
      };
    });
    frm.set_query("supplier_2", function () {
      return {
        filters: {
          type: 'Vendor'
        },
      };
    });
  },
  onload: function(frm) {
    if (frm.doc.main_category) {
      set_series(frm)
    }
  },
  refresh: function(frm) {
    if (frm.doc.docstatus == 1) {
      frappe.call({
        method: "jawa_logistics.jawa_logistics.doctype.file_info.file_info.get_voucher",
        args: {
          file_info: frm.doc.name,
        },
        callback(r) {
          if(r.message[0] == null){
            frm.add_custom_button(__('Purchase Invoice'),
            function() {
              frappe.model.open_mapped_doc({
                  method: "jawa_logistics.jawa_logistics.doctype.file_info.file_info.create_purchase_invoice",
                  frm: cur_frm,
              })
            }, __("Create"));
          }
          if(r.message[1] == null){
            frm.add_custom_button(__('Sales Invoice'),
            function() {
              frappe.model.open_mapped_doc({
                  method: "jawa_logistics.jawa_logistics.doctype.file_info.file_info.create_sales_invoice",
                  frm: cur_frm,
              })
            }, __("Create"));
          }
          if(r.message[2] == null){
            frm.add_custom_button(__('Delivery Note'),
            function() {
              frappe.model.open_mapped_doc({
                  method: "jawa_logistics.jawa_logistics.doctype.file_info.file_info.create_delivery_note",
                  frm: cur_frm,
              })
            }, __("Create")); 
          }
          }
    }); 
    }
  },
	shipper: function(frm) {
    erpnext.utils.get_address_display(frm, 'shipper_address_link', 'shipper_address');
    frappe.db.get_value("Supplier", {"name": frm.doc.shipper}, "default_currency", (r) => {
      frm.set_value("shipper_currency", r.default_currency);
      frm.refresh_field("shipper_currency");
    });

	},
  customer: function(frm) {
    erpnext.utils.get_address_display(frm, 'customer_address_link', 'customer_address');
    frappe.db.get_value("Customer", {"name": frm.doc.customer}, "default_currency", (r) => {
      frm.set_value("customer_currency", r.default_currency);
      frm.refresh_field("customer_currency");
    });

  },
  consignee: function(frm) {
    erpnext.utils.get_address_display(frm, 'consignee_address_link', 'consignee_address');
    frappe.db.get_value("Customer", {"name": frm.doc.consignee}, "default_currency", (r) => {
      frm.set_value("consignee_currency", r.default_currency);
      frm.refresh_field("consignee_currency");
    });

  },
  agent: function(frm) {
    erpnext.utils.get_address_display(frm, 'agent_address_link', 'agent_address');
    frappe.db.get_value("Supplier", {"name": frm.doc.agent}, "default_currency", (r) => {
      frm.set_value("agent_currency", r.default_currency);
      frm.refresh_field("agent_currency");
    });

  },
  supplier: function(frm) {
    erpnext.utils.get_address_display(frm, 'supplier_address_link', 'supplier_address');
    frappe.db.get_value("Supplier", {"name": frm.doc.supplier}, "default_currency", (r) => {
      frm.set_value("supplier_currency", r.default_currency);
      frm.refresh_field("supplier_currency");
    });

  },
  supplier_2: function(frm) {
    erpnext.utils.get_address_display(frm, 'supplier_address_link_2', 'supplier_address_2');
    frappe.db.get_value("Supplier", {"name": frm.doc.supplier_2}, "default_currency", (r) => {
      frm.set_value("supplier_currency_2", r.default_currency);
      frm.refresh_field("supplier_currency_2");
    });

  },
  main_category: function (frm) {
    if (frm.doc.main_category) {
      set_series(frm)
    }
  },
  main_services: function(frm) {
    if (frm.doc.main_services) {
      set_series(frm)
    }
  }
});

frappe.ui.form.on('Cost Info', {
  payee_type: function (frm, cdt, cdn) {
    var d = frappe.model.get_doc(cdt, cdn);
    if (d.payee_type == 'Agent') {
      frappe.model.set_value(cdt, cdn, "payee", frm.doc.agent);
      frappe.model.set_value(cdt, cdn, "currency", frm.doc.agent_currency);
    } else if (d.payee_type == 'Supplier') {
      frappe.model.set_value(cdt, cdn, "payee", frm.doc.supplier);
      frappe.model.set_value(cdt, cdn, "currency", frm.doc.supplier_currency);
    } else if (d.payee_type == 'Shipper') {
      frappe.model.set_value(cdt, cdn, "payee", frm.doc.shipper);
      frappe.model.set_value(cdt, cdn, "currency", frm.doc.shipper_currency);
    }
    frm.refresh_field("cost_info");
  },
  additional_service_charge: function (frm, cdt, cdn) {
    calculate_total_cost(frm, cdt, cdn)
  },
  cost_price: function (frm, cdt, cdn) {
    calculate_total_cost(frm, cdt, cdn)
  },
  vat: function (frm, cdt, cdn) {
    calculate_total_cost(frm, cdt, cdn)
  },
  total_cost_price: function (frm, cdt, cdn) {
    calculate_total_cost(frm, cdt, cdn)
  }
});

frappe.ui.form.on('Container Info', {
  create_delivery_note: function (frm, cdt, cdn) {
    var d = frappe.model.get_doc(cdt, cdn);
    if (!d.delivery_note) {
      frappe.call({
        method: "create_delivery_note_for_container_info",
        doc:frm.doc,
        args: {container:d },
        callback(r) {
          if (r.message)
            frappe.model.set_value(cdt, cdn, "delivery_note", r.message);
            frappe.msgprint(__(
							"Delivery Note successfully added {0}", [
								`<span class="underline">${frappe.utils.get_form_link(
									"Delivery Note",
									r.message,
									true
								)}<span>`,
							]
						))
        }

      });
    }else{
      frappe.msgprint(__(
        "Delivery Note already added {0}", [
          `<span class="underline">${frappe.utils.get_form_link(
            "Delivery Note",
            d.delivery_note,  
            true
          )}<span>`,
        ]
      ))    
    }
  }
});





var calculate_total_cost = function(frm, cdt, cdn) {
  var d = frappe.model.get_doc(cdt, cdn);
  var total_cost = (d.cost_price + d.additional_service_charge) * (1 + (d.vat/100))
  frappe.model.set_value(cdt, cdn, "total_cost_price", total_cost);
  frm.refresh_field("cost_info");
}

var set_series = function(frm) {
  if (frm.doc.main_category == 'Import' && frm.doc.main_services == 'Air') {
    frm.set_value('naming_series', 'AI')
  } else if (frm.doc.main_category == 'Import' && frm.doc.main_services == 'Ocean') {
    frm.set_value('naming_series', 'OI')
  } else if (frm.doc.main_category == 'Import' && frm.doc.main_services == 'Land') {
    frm.set_value('naming_series', 'LI')
  } else if (frm.doc.main_category == 'Export' && frm.doc.main_services == 'Air') {
    frm.set_value('naming_series', 'AE')
  } else if (frm.doc.main_category == 'Export' && frm.doc.main_services == 'Ocean') {
    frm.set_value('naming_series', 'OE')
  } else if (frm.doc.main_category == 'Export' && frm.doc.main_services == 'Land') {
    frm.set_value('naming_series', 'LE')
  } else if (frm.doc.main_category == 'Custom Clearance') {
    frm.set_value('naming_series', 'CC')
  } else if (frm.doc.main_category == 'Cross Trading') {
    frm.set_value('naming_series', 'CT')
  } else if (frm.doc.main_services == 'House Bill') {
    frm.set_value('naming_series', 'HB')
  }
}