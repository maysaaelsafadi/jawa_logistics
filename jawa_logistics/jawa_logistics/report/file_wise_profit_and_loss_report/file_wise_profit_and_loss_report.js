// Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
// License: GNU General Public License v3. See license.txt

frappe.query_reports["File Wise Profit and Loss Report"] = {
	"filters": [
		{
			"fieldname":"from_date",
			"label": __("From Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.add_months(frappe.datetime.get_today(), -1),
			"reqd": 1,
			"width": "60px"
		},
		{
			"fieldname":"to_date",
			"label": __("To Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.get_today(),
			"reqd": 1,
			"width": "60px"
		},
		{
		   "fieldname": "customer",
		   "fieldtype": "Link",
		   "label": "Customer",
		   "options": "Customer"
		  },
		{
		   "fieldname": "supplier",
		   "fieldtype": "Link",
		   "label": "supplier",
		   "options": "Supplier"
		  },
		{
   "fieldname": "main_category",
   "fieldtype": "Select",
   "label": "Main Category",
   "options": "\nImport\nExport\nCross Trading\nCustom Clearance"

  },
		{
   "fieldname": "main_services",
   "fieldtype": "Link",
   "label": "Main Services",
   "options": "Main Service"

  }

	]
}