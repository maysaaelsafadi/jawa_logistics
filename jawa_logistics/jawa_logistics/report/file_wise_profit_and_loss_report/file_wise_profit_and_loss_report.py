# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
# License: GNU General Public License v3. See license.txt
from __future__ import unicode_literals
import frappe
from frappe import _

def execute(filters=None):
    if not filters:
        return [], []
    if filters.from_date > filters.to_date:
        frappe.throw(_("From Date must be before To Date"))

    columns = get_columns()
    data = get_info(filters)

    return columns, data

def get_columns():
    columns = [

        {
            "label": _("Transaction Date"),
            "fieldname": "transaction_date",
            "fieldtype": "Date",
            "width": 100
        },
        {
            "label": _("Customer"),
            "fieldname": "customer",
            "fieldtype": "Link",
            "width": 100,
            "options": "Customer",
        },
        {
            "label": _("Customer Name"),
            "fieldname": "customer_name",
            "fieldtype": "Data",
        },
        {
            "label": _("Main Category"),
            "fieldname": "main_category",
            "fieldtype": "Data",
        },
        {
            "label": _("Main Services"),
            "fieldname": "main_services",
            "fieldtype": "Data",
        },
        {
            "label": _("Number of Sales Invoices"),
            "fieldname": "number_sales_invoices",
            "fieldtype": "Int",
            "width": 100
        },
        {
            "label": _("Total of Sales Invoices"),
            "fieldname": "total_sales_invoices",
            "fieldtype": "Float",
            "width": 100
        },
        {
            "label": _("Number of Purchase Invoices"),
            "fieldname": "number_purchase_invoices",
            "fieldtype": "Int",
            "width": 100
        },
        {
            "label": _("Total of Purchase Invoices"),
            "fieldname": "total_purchase_invoices",
            "fieldtype": "Float",
            "width": 100
        },
        {
            "label": _("Profit and Loss"),
            "fieldname": "profit_loss",
            "fieldtype": "Float",
            "width": 100
        }
    ]
    return columns

def get_info(filters):

        condition=get_condition(filters)

        all_list = frappe.db.sql("""select 
                                    f.transaction_date,
                                    f.name as file_info,
                                    f.customer,
                                    f.customer_name,
                                    f.main_category,
                                    f.main_services,
                                    
                                    (SELECT COUNT(*) FROM `tabSales Invoice` where docstatus=1 and file_info=f.name and posting_date between date('{0}') and date('{1}') ) as number_sales_invoices,
                                    (SELECT sum(grand_total) FROM `tabSales Invoice` where docstatus=1 and file_info=f.name and posting_date between date('{0}') and date('{1}') ) as total_sales_invoices,
                                    (SELECT COUNT(*) FROM `tabPurchase Invoice` where docstatus=1 and file_info=f.name) as number_purchase_invoices,
                                    (SELECT sum(grand_total) FROM `tabPurchase Invoice` where docstatus=1 and file_info=f.name) as total_purchase_invoices
                                     from `tabFile Info` as f  where 1 {2} """.format(filters.get("from_date"),filters.get("to_date"),condition), as_dict=1)
        return_array=[]
        for row in all_list:
            if not (row['total_sales_invoices']):
                row['total_sales_invoices']=0.0
            if not (row['number_sales_invoices']):
                row['number_sales_invoices']=0
            if not (row['number_purchase_invoices']):
                row['number_purchase_invoices']=0
            if not (row['total_purchase_invoices']):
                row['total_purchase_invoices']=0.0

            row['profit_loss']=float(row['total_sales_invoices'])-float(row['total_purchase_invoices'])
        return all_list

def get_condition(filters):
    cond=""
    if filters.get("customer") :
        cond+=" and f.customer='{0}'".format(filters.get("customer"))
    if filters.get("supplier") :
        cond+=" and f.agent='{0}'".format(filters.get("supplier"))
    if filters.get("main_category") :
        cond+=" and f.main_category='{0}'".format(filters.get("main_category"))
    if filters.get("main_services"):
        cond += " and f.main_services='{0}'".format(filters.get("main_services"))
    return cond



