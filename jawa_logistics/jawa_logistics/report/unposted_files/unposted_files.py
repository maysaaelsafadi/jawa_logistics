# Copyright (c) 2013, Jawa and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
from cProfile import label
import frappe
from frappe import msgprint, _

def execute(filters=None):
	conditions, filters = get_conditions(filters)
	columns = get_column()
	data = get_data(conditions,filters)

	# Chart Data
	chart_data = get_chart_data(conditions,filters)
	labels = []
	values = []

	for row in chart_data:
		labels.append(row.get("customer"))
		values.append(row.get("files"))
	chart = {'data':{'labels':labels,'datasets':[{'values':values}]},'type':'pie'}


	return columns,data, None, chart

def get_column():
	return [
		_("File Info#") + ":Link/File Info:100",
		_("Date") + ":Date:100",
		_("Company") + ":Link/Company:120",
		_("Customer") + ":Link/Customer:150",
		_("Customer Name") + ":Data:150",
		_("Agent") + ":Data:150",
		_("Shipper") + ":Link/Supplier:150",
		_("Shipper Name") + ":Data:150",
  		_("Supplier") + ":Link/Supplier:150",
		_("Supplier Name") + ":Data:150",
		_("Status") + ":Data:120",
		_("Main Category") + ":Data:120",
		_("Main Services") + ":Data:120",
		_("Booking Number") + ":Data:120"
	]

def get_data(conditions,filters):
	attendance = frappe.db.sql("""select 
		fi.name,
		fi.transaction_date,
		fi.company,
  		fi.customer,
  		fi.customer_name,
    	fi.agent_name,
		fi.shipper,
  		fi.shipper_name,
		fi.supplier,
  		fi.supplier_name,
		"Draft",
		fi.main_category,
		fi.main_services,
    	fi.booking_number
	from `tabFile Info` fi 
	where %s 
 	ORDER BY fi.name ASC"""%conditions, filters, as_list=1)
	return attendance

def get_chart_data(conditions,filters):
	attendance = frappe.db.sql("""select 
  		fi.customer,
		count(*) as files
	from `tabFile Info` fi 
	where %s 
	GROUP BY fi.customer
 	ORDER BY files DESC"""%conditions, filters, as_dict=1)
	return attendance
def get_conditions(filters):
	conditions = "  fi.docstatus = 0 "
	if filters.get("company"): conditions += " and fi.company = %(company)s"
	if filters.get("from_date"): conditions += " and fi.transaction_date >= %(from_date)s"
	if filters.get("to_date"): conditions += " and fi.transaction_date <= %(to_date)s"

	return conditions, filters

