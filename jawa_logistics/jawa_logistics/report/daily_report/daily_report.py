# Copyright (c) 2013, Jawa and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import msgprint, _

def execute(filters=None):
        conditions, filters = get_conditions(filters)
        columns = get_column()
        data = get_data(conditions,filters)
        return columns,data

def get_column():
        return [_("JOB #") + ":Link/File Info:100",
				_("Container") + ":Data:100",
                _("Date") + ":Date:100",
				_("Customer Name") + ":Data:150",
				_("Agent") + ":Data:150",
                _("Vendor") + ":Data:150",
				_("Shipper Name") + ":Data:150",
				_("Booking Number") + ":Data:150",
				_("Vessel Name") + ":Data:100",
				_("Commodity") + ":Data:100",
				_("B/L #") + ":Data:100",
				_("POL") + ":Data:100",
				_("POD") + ":Data:100",
				_("Description") + ":Data:150",
				_("Qty") + ":Float:100",
				_("Unit") + ":Link/UOM:100",
				_("C Gross Weight") + ":Float:150",
				_("C Volume") + ":Float:120",
				_("ETA") + ":Datetime:120",
				_("Volume") + ":Data:120",
				_("Tracking Info") + ":Data:120",
				_("Status") + ":Data:120"]

def get_data(conditions,filters):
        attendance = frappe.db.sql("""select fi.name,
	if(EXISTS(select parent from `tabContainer Info` where parent = fi.name),'Yes','No'),
	fi.transaction_date,fi.customer_name,fi.agent_name,
	fi.supplier_name,fi.shipper_name,fi.booking_number,fi.vessel_no,fi.main_services,
	(CASE
	    WHEN fi.main_services = "Air" THEN fi.air_waybill
		WHEN fi.main_services = "Ocean" THEN fi.bill_of_land
		WHEN fi.main_services = "Land" THEN fi.land_waybill
		WHEN fi.main_services = "House Bill" THEN fi.house_bill
		ELSE ""
	END),fi.pol,fi.pod,fii.description_of_goods, fii.qty, fii.unit, fii.c_gross_weight, fii.c_volume,
		fi.eta, fi.volume, (select status from `tabTracking Info` where parent = fi.name order by idx desc limit 1), 
	(CASE
	    WHEN fi.docstatus = 0 THEN 'Draft'
		WHEN fi.docstatus = 1 THEN 'Submitted'
		WHEN fi.docstatus = 2 THEN 'Cancelled'
		ELSE ""
	END)
			from `tabFile Info` fi, `tabFile Info Item` fii where fi.name = fii.parent
			%s ORDER BY fi.name ASC;"""%conditions, filters, as_list=1)
        return attendance

def get_conditions(filters):
	conditions = ""
	if filters.get("company"): conditions += " and fi.company = %(company)s"
	if filters.get("from_date"): conditions += " and fi.transaction_date >= %(from_date)s"
	if filters.get("to_date"): conditions += " and fi.transaction_date <= %(to_date)s"
	if filters.get("main_services"): conditions += " and fi.main_services = %(main_services)s"
	if filters.get("status") and filters.get("status") == "Submitted":
		conditions += " and fi.docstatus = 1"
	if filters.get("status") and filters.get("status") == "Draft":
		conditions += " and fi.docstatus = 0"
	if filters.get("status") and filters.get("status") == "Cancelled":
		conditions += " and fi.docstatus = 2"		

	return conditions, filters

