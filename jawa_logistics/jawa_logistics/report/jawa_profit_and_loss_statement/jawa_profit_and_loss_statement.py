# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
# License: GNU General Public License v3. See license.txt
from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.utils import flt, cstr
from erpnext.accounts.report.financial_statements import (get_period_list, get_columns, get_accounts, get_appropriate_currency, get_appropriate_currency, \
    calculate_values, accumulate_values_into_parents, prepare_data, add_total_row, filter_out_zero_value_rows, \
        convert_to_presentation_currency, get_currency, sort_accounts)
from jawa_logistics.reports.trial_balance import get_additional_conditions
def execute(filters=None):
	period_list = get_period_list(
		filters.from_fiscal_year,
		filters.to_fiscal_year,
		filters.period_start_date,
		filters.period_end_date,
		filters.filter_based_on,
		filters.periodicity,
		company=filters.company,
	)

	liability = {}
	against_account = []
	invoices = []
	if(filters.get("customer") or filters.get("file_info")):
		if filters.get("customer"):
			against_account.append(filters.get("customer"))
			cond = " customer = '{}'".format(filters.get("customer"))
			for row in frappe.db.sql(""" SELECT distinct(shipper) as shipper from `tabFile Info` where %s """ %cond, as_dict=True):
				against_account.append(row.get("shipper"))
		if filters.get("file_info"):
			cond = " name = '{}'".format(filters.get("file_info"))
			for row in frappe.db.sql(""" SELECT shipper, customer from `tabFile Info` where %s """ %cond, as_dict=True):
				against_account.append(row.get("shipper"))
				against_account.append(row.get("customer"))

			for invoice in frappe.get_list("Sales Invoice",{"docstatus":1,"file_info":filters.get("file_info")}):
				invoices.append(invoice.get("name"))

			for invoice in frappe.get_list("Purchase Invoice",{"docstatus":1,"file_info":filters.get("file_info")}):
				invoices.append(invoice.get("name"))

		liability = get_data(filters.company, "Liability", "Debit", period_list, filters=filters,
			accumulated_values=filters.accumulated_values,
			ignore_closing_entries=True, ignore_accumulated_values_for_fy= True,against_account=against_account,invoices=invoices)


	income = get_data(filters.company, "Income", "Credit", period_list, filters = filters,
		accumulated_values=filters.accumulated_values,
		ignore_closing_entries=True, ignore_accumulated_values_for_fy= True,against_account=against_account,invoices=invoices)

	expense = get_data(filters.company, "Expense", "Debit", period_list, filters=filters,
		accumulated_values=filters.accumulated_values,
		ignore_closing_entries=True, ignore_accumulated_values_for_fy= True,against_account=against_account,invoices=invoices)


	# liability = get_data(filters.company, "Liability", "Debit", period_list, filters=filters,
	# 	accumulated_values=filters.accumulated_values,
	# 	ignore_closing_entries=True, ignore_accumulated_values_for_fy= True)

	net_profit_loss = get_net_profit_loss(income, expense, period_list, filters.company, filters.presentation_currency,liability = liability)

	data = []
	data.extend(income or [])
	data.extend(expense or [])
	data.extend(liability or [])
 
	if net_profit_loss:
		data.append(net_profit_loss)

	columns = get_columns(filters.periodicity, period_list, filters.accumulated_values, filters.company)
	columns[2]["width"] = 300
	chart = get_chart_data(filters, columns, income, expense, net_profit_loss, liability)

	return columns, data, None, chart

def get_net_profit_loss(income, expense, period_list, company, currency=None, consolidated=False, liability=dict()):
	total = 0
	net_profit_loss = {
		"account_name": "'" + _("Profit for the year") + "'",
		"account": "'" + _("Profit for the year") + "'",
		"warn_if_negative": True,
		"currency": currency or frappe.get_cached_value('Company',  company,  "default_currency")
	}

	has_value = False

	for period in period_list:
		key = period if consolidated else period.key
		total_income = flt(income[-2][key], 3) if income else 0
		total_expense = flt(expense[-2][key], 3) if expense else 0
		liability = flt(liability[-2][key], 3) if liability else 0

		net_profit_loss[key] = total_income - (total_expense + liability)

		if net_profit_loss[key]:
			has_value=True

		total += flt(net_profit_loss[key])
		net_profit_loss["total"] = total

	if has_value:
		return net_profit_loss

def get_chart_data(filters, columns, income, expense, net_profit_loss, liability):
	labels = [d.get("label") for d in columns[2:]]

	income_data, expense_data, net_profit, liability_data = [], [], [], []

	for p in columns[2:]:
		if income:
			income_data.append(income[-2].get(p.get("fieldname")))
		if expense:
			expense_data.append(expense[-2].get(p.get("fieldname")))
		if liability:
			liability_data.append(liability[-2].get(p.get("fieldname")))

		if net_profit_loss:
			net_profit.append(net_profit_loss.get(p.get("fieldname")))

	datasets = []
	if income_data:
		datasets.append({'name': _('Income'), 'values': income_data})
	if expense_data:
		datasets.append({'name': _('Expense'), 'values': expense_data})
	if liability_data:
		datasets.append({'name': _('Liability'), 'values': liability_data})
	if net_profit:
		datasets.append({'name': _('Net Profit/Loss'), 'values': net_profit})

	chart = {
		"data": {
			'labels': labels,
			'datasets': datasets
		}
	}

	if not filters.accumulated_values:
		chart["type"] = "bar"
	else:
		chart["type"] = "line"

	chart["fieldtype"] = "Currency"

	return chart

def get_data(
		company, root_type, balance_must_be, period_list, filters=None,
		accumulated_values=1, only_current_fiscal_year=True, ignore_closing_entries=False,
		ignore_accumulated_values_for_fy=False , total = True, against_account=[],invoices=[]):

	accounts = get_accounts(company, root_type)
	if not accounts:
		return None

	accounts, accounts_by_name, parent_children_map = filter_accounts(accounts)

	company_currency = get_appropriate_currency(company, filters)

	gl_entries_by_account = {}
	for root in frappe.db.sql("""select lft, rgt from tabAccount
			where root_type=%s and ifnull(parent_account, '') = ''""", root_type, as_dict=1):

		set_gl_entries_by_account(
			company,
			period_list[0]["year_start_date"] if only_current_fiscal_year else None,
			period_list[-1]["to_date"],
			root.lft, root.rgt, filters,
			gl_entries_by_account, ignore_closing_entries=ignore_closing_entries, against_account=against_account,invoices=invoices
		)

	calculate_values(
		accounts_by_name, gl_entries_by_account, period_list, accumulated_values, ignore_accumulated_values_for_fy)
	accumulate_values_into_parents(accounts, accounts_by_name, period_list)
	out = prepare_data(accounts, balance_must_be, period_list, company_currency)
	out = filter_out_zero_value_rows(out, parent_children_map)

	if out and total:
		add_total_row(out, root_type, balance_must_be, period_list, company_currency)

	return out

def set_gl_entries_by_account(
		company, from_date, to_date, root_lft, root_rgt, filters, gl_entries_by_account, ignore_closing_entries=False, against_account=[],invoices=[]):
	"""Returns a dict like { "account": [gl entries], ... }"""

	additional_conditions = get_additional_conditions(from_date, ignore_closing_entries, filters)

	accounts = frappe.db.sql_list("""select name from `tabAccount`
		where lft >= %s and rgt <= %s and company = %s""", (root_lft, root_rgt, company))

	if accounts:
		additional_conditions += " and account in ({})"\
			.format(", ".join([frappe.db.escape(d) for d in accounts]))

		gl_filters = {
			"company": company,
			"from_date": from_date,
			"to_date": to_date,
			"finance_book": cstr(filters.get("finance_book"))
		}

		if against_account:
			additional_conditions += " and against in ({})"\
				.format(", ".join([frappe.db.escape(d) for d in against_account]))

		if invoices:
			additional_conditions += " and voucher_no in ({})"\
				.format(", ".join([frappe.db.escape(d) for d in invoices]))


		if filters.get("include_default_book_entries"):
			gl_filters["company_fb"] = frappe.db.get_value("Company",
				company, 'default_finance_book')

		for key, value in filters.items():
			if value:
				gl_filters.update({
					key: value
				})

		gl_entries = frappe.db.sql("""select posting_date, account, debit, credit, is_opening, fiscal_year, debit_in_account_currency, credit_in_account_currency, account_currency from `tabGL Entry`
			where company=%(company)s
			{additional_conditions}
			and posting_date <= %(to_date)s
			order by account, posting_date""".format(additional_conditions=additional_conditions), gl_filters, as_dict=True,debug=True) #nosec
		if filters and filters.get('presentation_currency'):
			convert_to_presentation_currency(gl_entries, get_currency(filters))

		for entry in gl_entries:
			gl_entries_by_account.setdefault(entry.account, []).append(entry)

		return gl_entries_by_account

def filter_accounts(accounts, depth=20, custom_accounts=[]):
	parent_children_map = {}
	accounts_by_name = {}
	for d in accounts:
		accounts_by_name[d.name] = d
		parent_children_map.setdefault(d.parent_account or None, []).append(d)

	filtered_accounts = []

	def add_to_list(parent, level):
		if level < depth:
			children = parent_children_map.get(parent) or []
			sort_accounts(children, is_root=True if parent==None else False)

			for child in children:
				child.indent = level
				filtered_accounts.append(child)
				add_to_list(child.name, level + 1)

	add_to_list(None, 0)

	return filtered_accounts, accounts_by_name, parent_children_map