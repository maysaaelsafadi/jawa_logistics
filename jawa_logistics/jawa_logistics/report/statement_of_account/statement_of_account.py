# Copyright (c) 2013, Jawa and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import msgprint, _, scrub


def execute(filters=None):
	if not filters: filters = {}

	columns, data = [], []
	columns = get_columns()
	data = get_data(filters)
	# if ddata:

	# 	for inv in ddata:
	# 		row = [frappe.utils.getdate(inv.posting_date),inv.voucher_no, 0, inv.voucher_type, 0, 0, 0]

	# 		data.append(row)

	return columns, data


def get_columns():
	columns = [
		_("Date") + ":Date:100", _("Voucher") + "::150", _("Narration") + "::100",
		_("Reference Details") + ":Data:150", _("Invoice Amt") + "::100", _("Adjustments") + "::100",_("Balance") + "::90"
	]
	return columns


# def get_data(filters):
# 	conditions, filters = get_conditions(filters)
# 	return frappe.db.sql("""select emp.name as employee ,emp.employee_name, emp.department, emp.employee_type, att.for_month, \
# 		att.from_date, att.to_date,(30 - att.total_working_hours) as absent_days, att.hourly_deduction as absent_hrs, \
# 		att.creation as posting_date from `tabSales Invoice` where 1 %s order by emp.name""" % conditions, filters, as_dict=1)


def get_data(filters):
	data = []
	# get all the GL entries filtered by the given filters

	# conditions, values = get_conditions()
	# conditions, filters = get_conditions(filters)


	# if self.filters.show_future_payments:
	# 	values.insert(2, self.filters.report_date)

	# 	date_condition = """AND (posting_date <= %s
	# 		OR (against_voucher IS NULL AND DATE(creation) <= %s))"""
	# else:
		# date_condition = "AND posting_date <=%s"

	# if filters.get(scrub(filters.party_type)):
	# 	select_fields = "debit_in_account_currency as debit, credit_in_account_currency as credit"
	# else:
	# 	select_fields = "debit, credit"
	si_list = frappe.db.sql("""
				select name, due_date, po_no, posting_date, grand_total
				from `tabSales Invoice`
				where posting_date between %s and %s
			""",(filters.from_date, filters.to_date), as_dict=1)

	for d in si_list:
		data.append([frappe.utils.getdate(d.posting_date),d.name, d.description, 'New Reference', d.grand_total, 0.0, d.grand_total])

		det = frappe.db.sql("""
			select
				name, posting_date, account, party_type, party, voucher_type, voucher_no, cost_center,
				against_voucher_type, against_voucher, account_currency, credit,remarks
			from
				`tabGL Entry`
			where
				against_voucher = %s 
				and against_voucher_type = 'Sales Invoice'
				and voucher_type != 'Sales Invoice'
				and docstatus < 2
				and party_type='Customer'
				and (party is not null and party != '')
				   """,d.name, as_dict=True)
		for de in det:
				data.append([frappe.utils.getdate(de.posting_date), de.voucher_no, de.remarks , de.voucher_type, 0.0,  de.credit, d.grand_total-de.credit])

	return data

def get_invoice_details(self):
		self.invoice_details = frappe._dict()
		if self.filters.party_type == "Customer":
			si_list = frappe.db.sql("""
				select name, due_date, po_no
				from `tabSales Invoice`
				where posting_date between %s and %s
			""",(self.filters.from_date,self.filters.to_date), as_dict=1)
			for d in si_list:
				self.invoice_details.setdefault(d.name, d)

			# Get Sales Team
			if self.filters.show_sales_person:
				sales_team = frappe.db.sql("""
					select parent, sales_person
					from `tabSales Team`
					where parenttype = 'Sales Invoice'
				""", as_dict=1)
				for d in sales_team:
					self.invoice_details.setdefault(d.parent, {})\
						.setdefault('sales_team', []).append(d.sales_person)

		if self.filters.party_type == "Supplier":
			for pi in frappe.db.sql("""
				select name, due_date, bill_no, bill_date
				from `tabPurchase Invoice`
				where posting_date <= %s
			""", self.filters.report_date, as_dict=1):
				self.invoice_details.setdefault(pi.name, pi)

		# Invoices booked via Journal Entries
		journal_entries = frappe.db.sql("""
			select name, due_date, bill_no, bill_date
			from `tabJournal Entry`
			where posting_date <= %s
		""", self.filters.report_date, as_dict=1)

		for je in journal_entries:
			if je.bill_no:
				self.invoice_details.setdefault(je.name, je)


def get_conditions(filters):
	conditions = ""

	if filters.get("from_date"): conditions += " and att.from_date >= %(from_date)s"
	if filters.get("to_date"): conditions += " and att.to_date <= %(to_date)s"
	if filters.get("employee_type"): conditions += " and emp.employee_type = %(employee_type)s"
	if filters.get("employee"): conditions += " and emp.name = %(employee)s"
	if filters.get("for_month"): conditions += " and att.for_month = %(for_month)s"

	return conditions, filters

