frappe.pages['sales-dashboard'].on_page_load = function(wrapper) {

	new erpnext.SalesDashboard(wrapper);
}

erpnext.SalesDashboard = class SalesDashboard {

	constructor(wrapper) {

		this.page =  frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Sales Dashboard',
		single_column: true
	});


		$(frappe.render_template("sales_dashboard", this)).appendTo($(wrapper).find(".page-body"))
		this.wrapper = wrapper;
		const me = this;
		me.page.add_field({
			fieldtype: 'Date',
			label: __('From Date'),
			fieldname: 'from_date',
			default: frappe.defaults.get_user_default("year_start_date"),
			reqd: 1,
			onchange: function() {
				if (this.value) {
					me.from_date = this.value;
					me.get_data();
				} else {
					me.from_date = null;

				}
			}
		});
		me.page.add_field({
			fieldtype: 'Date',
			label: __('To Date'),
			fieldname: 'to_date',
			default: frappe.defaults.get_user_default("year_end_date"),
			reqd: 1,
			onchange: function() {
				if (this.value) {
					me.to_date = this.value;
					me.get_data();
				} else {
					me.to_date = null;

				}
			}
		});
		this.get_data();
	}
	get_data() {
		var from_date = this.from_date;
		var to_date = this.to_date;

		frappe.call({
			method: "jawa_logistics.jawa_logistics.page.sales_dashboard.sales_dashboard.file_wise_revenue",
			args: {
				from_date: from_date,
				to_date: to_date
			},
			callback: function(r) {
				if(!r.exc) {
					if (!r.message) {
						console.log(r.message);
					} else {
						if(r.message.labels.length){
							let chart = new frappe.Chart("#chart1", {
								title: "Monthly File-wise Revenue",
								height: 400,
								data: r.message,
								type: 'pie',
								barOptions: {
									stacked: 1
								},
							});
						}
					}
				}
			}
		});

		frappe.call({
			method: "jawa_logistics.jawa_logistics.page.sales_dashboard.sales_dashboard.department_wise_files",
			callback: function(r) {
				if(!r.exc) {
					if (!r.message) {
						console.log(r.message);

					} else {
						if(r.message.labels.length){
							let chart = new frappe.Chart("#chart2", {
								title: "Department Wise Files",
								height: 400,
								data: r.message,
								type: 'pie',
								barOptions: {
									stacked: 1
								},
							});
						}
					}
				}
			}
		});
		frappe.call({
			method: "jawa_logistics.jawa_logistics.page.sales_dashboard.sales_dashboard.get_top_5_count",
			callback: function(r) {
				if(!r.exc) {
					if (!r.message) {
						console.log(r.message);

					} else {
						if(r.message.labels.length){
							let chart = new frappe.Chart("#chart3", {
								title: "Top Customers",
								height: 400,
								data: r.message,
								type: 'bar',
								barOptions: {
									stacked: 1
								},
							});
						}
					}
				}
			}
		});

		frappe.call({
			method: "jawa_logistics.jawa_logistics.page.sales_dashboard.sales_dashboard.get_top_5_revenue",
			args: {
				from_date: from_date,
				to_date: to_date
			},
			callback: function(r) {
				if(!r.exc) {
					if (!r.message) {
						console.log(r.message);

					} else {
						if(r.message.labels.length){
							let chart = new frappe.Chart("#chart4", {
								title: "Top Customers Revenue",
								height: 400,
								data: r.message,
								type: 'bar',
								barOptions: {
									stacked: 1
								},
							});
						}
					}
				}
			}
		});
        frappe.call({
			method: "jawa_logistics.jawa_logistics.page.sales_dashboard.sales_dashboard.get_top_5_gross_profit",
			args: {
				from_date: from_date,
				to_date: to_date
			},
			callback: function(r) {
				if(!r.exc) {
					if (!r.message) {
						console.log(r.message);

					} else {
						if(r.message.labels.length){
							let chart = new frappe.Chart("#chart5", {
								title: "Top Customers By Gross Profit",
								height: 400,
								data: r.message,
								type: 'bar',
								barOptions: {
									stacked: 1
								},
							});
						}
					}
				}
			}
		});
	}

}
