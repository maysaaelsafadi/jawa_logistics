from cProfile import label
import frappe
from frappe.utils import getdate

DATE_FORMAT = "%Y-%m-%d"
import datetime


@frappe.whitelist()
def file_wise_revenue(from_date=None, to_date=None):
    labels = []
    values = []

    if not from_date:
        dt = getdate()
        date = datetime.date(dt.year, 1, 1)
        from_date = date.strftime(DATE_FORMAT)
    if not to_date:
        to_date = getdate()

    for row in frappe.db.sql(""" SELECT
			ifnull(sum(si.net_total),0) as total_amount, fi.main_services as service
		FROM
			`tabFile Info` fi
		LEFT JOIN
			`tabSales Invoice` si 
			ON
				si.file_info = fi.name and si.docstatus < 2 and si.file_info is not null 
			and si. posting_date between date('%s') and date('%s')

			where
			fi.docstatus < 2

		GROUP BY
			fi.main_services""" % (from_date, to_date), as_dict=True):
        labels.append(row.get("service"))
        values.append(abs(row.get("total_amount")))
    return {
        "labels": labels,
        "datasets": [
            {
                "values": values
            }
        ]
    }


@frappe.whitelist()
def department_wise_files():
    labels = []
    values = []
    for row in frappe.db.sql("""
		SELECT 
			CONCAT(fi.main_services, ' ', fi.main_category) AS department,
			COUNT(fi.name) AS count
		FROM
			`tabFile Info` fi
		WHERE
			fi.docstatus < 2
		GROUP BY department
		ORDER BY count DESC
		""", as_dict=True):
        labels.append(row.get("department"))
        values.append((row.get("count")))
    return {
        "labels": labels,
        "datasets": [
            {
                "values": values
            }
        ]
    }


@frappe.whitelist()
def get_top_5_count():
    labels = []
    values = []
    for row in frappe.db.sql("""
		SELECT 
			customer_name as customer,
			COUNT(fi.name) AS count
		FROM
			`tabFile Info` fi
		WHERE
			fi.docstatus < 2
		GROUP BY customer
		ORDER BY count DESC
		LIMIT 5
		""", as_dict=True):
        labels.append(row.get("customer"))
        values.append((row.get("count")))
    return {
        "labels": labels,
        "datasets": [
            {
                "values": values
            }
        ]
    }


@frappe.whitelist()
def get_top_5_revenue(from_date=None, to_date=None):
    if not from_date:
        dt = getdate()
        date = datetime.date(dt.year, 1, 1)
        from_date = date.strftime(DATE_FORMAT)
    if not to_date:
        to_date = getdate()

    labels = []
    values = []
    for row in frappe.db.sql(""" SELECT
			ifnull(sum(si.net_total),0) as total_amount, fi.customer_name as customer
		FROM
			`tabFile Info` fi
		LEFT JOIN
			`tabSales Invoice` si
			ON
				si.file_info = fi.name and si.customer = fi.customer and si.docstatus < 2 and si.file_info is not null
			and si.posting_date between date('%s') and date('%s') 
		where 
		fi.docstatus < 2

		GROUP BY
			fi.customer
   		ORDER BY total_amount DESC
     	LIMIT 5 """ % (from_date, to_date), as_dict=True):
        labels.append(row.get("customer"))
        values.append(abs(row.get("total_amount")))
    return {
        "labels": labels,
        "datasets": [
            {
                "values": values
            }
        ]
    }


@frappe.whitelist()
def get_top_5_gross_profit(from_date=None, to_date=None):
    if not from_date:
        dt = getdate()
        date = datetime.date(dt.year, 1, 1)
        from_date = date.strftime(DATE_FORMAT)
    if not to_date:
        to_date = getdate()

    labels = []
    values = []
    for row in frappe.db.sql(""" SELECT
		ifnull((
			ifnull(sum(si.net_total),0)- ifnull(sum(pi.net_total),0)),0) as total_amount,
			 fi.customer_name as customer
		FROM
			`tabFile Info` fi
		LEFT JOIN `tabSales Invoice` si ON si.file_info = fi.name and si.customer = fi.customer and si.docstatus < 2 and si.file_info is not null
         and si.posting_date between date('%s') and date('%s')

		LEFT JOIN `tabPurchase Invoice` pi ON pi.file_info = fi.name and pi.customer = fi.customer and pi.docstatus < 2 and pi.file_info is not null
         and pi.posting_date between date('%s') and date('%s')

where fi.docstatus < 2
GROUP BY fi.customer
ORDER BY total_amount DESC
LIMIT 5 """ % (from_date, to_date, from_date, to_date), as_dict=True):
        labels.append(row.get("customer"))
        values.append(abs(row.get("total_amount")))
    return {
        "labels": labels,
        "datasets": [
            {
                "values": values
            }
        ]
    }
