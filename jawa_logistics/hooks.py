# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "jawa_logistics"
app_title = "Jawa Logistics"
app_publisher = "Jawa"
app_description = "Logistics App for Jawa"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "mesa_safd@hotmail.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
app_include_css = "/assets/jawa_logistics/css/jawa_logistics.css"
# app_include_js = "/assets/jawa_logistics/js/jawa_logistics.js"

app_reports_js = {
	"Accounts Receivable Summary": "public/js/accounts_receivable_summary.js",
	"Accounts Receivable": "public/js/accounts_receivable.js",
	"Accounts Payable Summary": "public/js/accounts_payable_summary.js",
	"Accounts Payable": "public/js/accounts_payable.js"
}   


# include js, css files in header of web template
# web_include_css = "/assets/jawa_logistics/css/jawa_logistics.css"
# web_include_js = "/assets/jawa_logistics/js/jawa_logistics.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "jawa_logistics.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "jawa_logistics.install.before_install"
# after_install = "jawa_logistics.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "jawa_logistics.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

override_doctype_class = {
	"Sales Invoice": "jawa_logistics.controllers.accounts_controller.CustomSalesInvoice",
	"Purchase Invoice": "jawa_logistics.controllers.accounts_controller.CustomPurchaseInvoice",
	'Journal Entry':'jawa_logistics.controllers.journal_entry.CustomJournalEntry',
	'Payment Entry':'jawa_logistics.controllers.payment_entry.CustomPaymentEntry',
     "Delivery Note": "jawa_logistics.controllers.delivery_note.CustomDeliveryNote",
 
}

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }
# fixtures = ["Custom Script"]
doctype_js = {
	"Purchase Invoice" : "public/js/purchase_invoice.js",
    "Sales Invoice": "public/js/sales_invoice.js",
    "Customer": "public/js/customer.js",
    "Asset": "public/js/asset.js",
    "Supplier": "public/js/supplier.js",
    "Delivery Note": "public/js/delivery_note.js",
    "Payment Entry": "public/js/payment_entry.js",
    "Payment Request": "public/js/payment_request.js"
}

doc_events = {
    "Sales Invoice": {
        "on_submit":  ["jawa_logistics.controllers.base_controller.validate_controller","jawa_logistics.controllers.sales_invoice.on_submit"],
        "before_save":"jawa_logistics.controllers.base_controller.before_save",
        "before_insert":"jawa_logistics.controllers.sales_invoice.before_insert"
    },
    "Purchase Invoice":{
        "on_submit": "jawa_logistics.controllers.base_controller.validate_controller",
        "before_save": "jawa_logistics.controllers.base_controller.before_save"
    },
    "Journal Entry": {
		"validate": "jawa_logistics.hook.journal_entry.update_In_words",
  		"before_submit": "jawa_logistics.hook.journal_entry.before_submit",
    },
    "Payment Entry": {
		"validate": "jawa_logistics.hook.payment_entry.validate",
		"before_save": "jawa_logistics.hook.payment_entry.before_save",
		"before_insert": "jawa_logistics.hook.payment_entry.before_insert",
		"on_update": "jawa_logistics.hook.payment_entry.on_update"
    },
    "Customer":{
        "before_save":"jawa_logistics.hook.customer.validate_customer"
    }
    
}
# Scheduled Tasks
# ---------------

scheduler_events = {
# 	"all": [
# 		"jawa_logistics.tasks.all"
# 	],
# 	"daily": [
# 		"jawa_logistics.tasks.daily"
# 	],
	"hourly": [
		"jawa_logistics.auto_updat_gl_finance_book"
	],
# 	"weekly": [
# 		"jawa_logistics.tasks.weekly"
# 	]
# 	"monthly": [
# 		"jawa_logistics.tasks.monthly"
# 	]
}

# Testing
# -------

# before_tests = "jawa_logistics.install.before_tests"

# Overriding Methods
# ------------------------------
#
override_whitelisted_methods = {
	"frappe.desk.query_report.run": "jawa_logistics.hook.whitelisted.run",
    "erpnext.accounts.doctype.payment_request.payment_request.make_payment_request":"jawa_logistics.hook.whitelisted.make_payment_request",
    "erpnext.accounts.doctype.payment_entry.payment_entry.get_payment_entry":"jawa_logistics.hook.whitelisted.get_payment_entry",
    "erpnext.accounts.doctype.payment_request.payment_request.make_payment_entry":"jawa_logistics.hook.whitelisted.make_payment_entry",
	"erpnext.accounts.doctype.payment_entry.payment_entry.get_outstanding_reference_documents" : "jawa_logistics.hook.whitelisted.get_outstanding_reference_documents",
	"frappe.desk.query_report.get_script": "jawa_logistics.hook.whitelisted.get_script",
}
#
# each overriding function accepts a `data` argument;
# generated from the base implementation of the doctype dashboard,
# along with any modifications made in other Frappe apps
override_doctype_dashboards = {
	"Customer": "jawa_logistics.dashboard.customer_dashboard.get_data"
}
