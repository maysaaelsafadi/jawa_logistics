# -*- coding: utf-8 -*-
from __future__ import unicode_literals

__version__ = '0.0.1'

import frappe


@frappe.whitelist()
def get_item_tax_template(item_code, is_sales):
	# return frappe.get_value("Item Tax", {"parent" : item_code, "is_sales": is_sales}, "item_tax_template")
	return frappe.get_value("Item Tax Template", { "is_sales": is_sales}, "name")

def auto_updat_gl_finance_book():
	frappe.db.sql(""" Update `tabGL Entry` set finance_book=(select finance_book from `tabPayment Entry` where name=`tabGL Entry`.voucher_no) where finance_book is null and voucher_type='Payment Entry' """)	
	frappe.db.sql(""" update `tabGL Entry` set finance_book=(select finance_book from `tabJournal Entry` where name=`tabGL Entry`.voucher_no) where finance_book is null and voucher_type='Journal Entry' """)
	frappe.db.sql(""" update `tabGL Entry` set finance_book=(select finance_book from `tabSales Invoice` where name=`tabGL Entry`.voucher_no) where finance_book is null and voucher_type='Sales Invoice' """)
	frappe.db.sql(""" update `tabGL Entry` set finance_book=(select finance_book from `tabPurchase Invoice` where name=`tabGL Entry`.voucher_no) where finance_book is null and voucher_type='Purchase Invoice' """)
	print("GL updated")