frappe.ui.form.on('Customer', {
	type: function(frm) {
		if (frm.doc.type == 'Customer') {
		    frm.set_value('naming_series', '2')
		} else if (frm.doc.type == 'Consignee') {
		    frm.set_value('naming_series', '3')
		}
	}
})