frappe.ui.form.on('Payment Request', {
    refresh: function(frm) {
        frm.set_df_property("currency", "read_only", 0);
        frm.refresh_field("currency");
    }
});