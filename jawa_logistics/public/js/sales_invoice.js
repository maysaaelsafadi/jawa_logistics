frappe.ui.form.on('Sales Invoice', {
	onload: function(frm) {
		if (frm.doc.docstatus === 0) {
			set_timeout(frm);
			frm.clear_custom_buttons();
		}

		frm.set_query("item_tax_template", "items",  function(doc, cdt, cdn) {
			return {
				filters: {'is_sales': 1}
			}
		});
	},
	refresh: function(frm) {
		if (frm.doc.docstatus === 0) {
			set_timeout(frm);
			frm.clear_custom_buttons();
		}
	},
	onload_post_render: function(frm) {
		if (frm.doc.docstatus === 0) {
			set_timeout(frm);
			frm.clear_custom_buttons();
		}    
	},
	setup: function(frm) {
		frm.set_query("file_info", function () {
			return {
				filters: {
					docstatus: 1
				},
			};
		});

	}
})
frappe.ui.form.on('Sales Invoice Item', {
	// item_code: function (frm, cdt, cdn) {
	//     var d = frappe.model.get_doc(cdt, cdn);
	//     if (d.item_code) {
	//         frappe.call({
	//             method: "jawa_logistics.get_item_tax_template",
	//             args: {
	//                 item_code: d.item_code,
	//                 is_sales: 1
	//             },
	//             callback: function(r) {
	//                 if(r.message) {
	//                     console.log(r.message)
	//                     frappe.model.set_value(cdt, cdn, 'item_tax_template', r.message)
	//                 }
	//             }
	//         });
	//     }
	// },

	item_tax_template_c: function (frm, cdt, cdn) {
		var row = locals[cdt][cdn];
		frappe.model.set_value(cdt, cdn, 'item_tax_template', row.item_tax_template_c);
	},
	service_start_date: function (frm, cdt, cdn) {
		set_end_month(frm, cdt, cdn)
	},
	no_of_months: function (frm, cdt, cdn) {
		set_end_month(frm, cdt, cdn)
	},
	
})

var set_timeout = function(frm) {
	setTimeout(() => {
			
			frm.remove_custom_button("Sales Order", 'Get items from');
			frm.remove_custom_button("Delivery Note", 'Get items from'); 
			frm.remove_custom_button("Quotation", 'Get items from'); 
			
		}, 500);
}

var set_end_month = function(frm, cdt, cdn) {
	var d = frappe.model.get_doc(cdt, cdn);
	if (d.no_of_months && d.service_start_date) {
		var start_date = new Date(d.service_start_date);
		var end_date = new Date(start_date.setMonth(start_date.getMonth() + d.no_of_months));
		frappe.model.set_value(cdt, cdn, "service_end_date", end_date);
	}
}

frappe.ui.form.on("Sales Invoice", {
	"file_info": function(frm) {
	if (frm.doc.file_info){
		frappe.model.with_doc("File Info", frm.doc.file_info, function() {
			var tabletransfer= frappe.model.get_doc("File Info", frm.doc.file_info);
				frm.set_value("house_bill",tabletransfer.house_bill);
				frm.set_value("land_waybill",tabletransfer.land_waybill);
				frm.set_value("air_waybill",tabletransfer.air_waybill);
				frm.set_value("bill_of_land",tabletransfer.bill_of_land);
				frm.set_value("main_service",tabletransfer.main_services);
		});
	}
  }
  });

cur_frm.set_query("item_tax_template_c", "items", function (doc, cdt, cdn) {
	var d = locals[cdt][cdn];
	return {
		filters: [
			['Item Tax Template', 'is_sales', '=', 1]
		]
	};
});