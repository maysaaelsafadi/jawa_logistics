frappe.ui.form.on("Delivery Note", {
    "file_info": function(frm) {
  if (frm.doc.file_info){
        frappe.model.with_doc("File Info", frm.doc.file_info, function() {
        cur_frm.clear_table("container_info");
        cur_frm.clear_table("file_info_item");
            var tabletransfer= frappe.model.get_doc("File Info", frm.doc.file_info);
            $.each(tabletransfer.container, function(index, row){
              var d = frm.add_child("container_info");
                d.container_type = row.container_type;
                d.container_number = row.container_number;
                d.seal_number = row.seal_number;
                d.description = row.description;
                frm.refresh_field("container_info");
            });
            $.each(tabletransfer.item, function(index, row){
              var d = frm.add_child("file_info_item");
                d.description_of_goods = row.description_of_goods;
                d.qty = row.qty;
                d.unit = row.unit;
                d.c_gross_weight = row.c_gross_weight;
                d.c_volume = row.c_volume;
                frm.refresh_field("file_info_item");
            });
            if(tabletransfer.main_services == "Ocean"){
                frm.set_value("waybill",tabletransfer.bill_of_land);
            }
            if(tabletransfer.main_services == "Air"){
                frm.set_value("waybill",tabletransfer.air_waybill);
            }
            if(tabletransfer.main_services == "Land"){
                frm.set_value("waybill",tabletransfer.land_waybill);
            }
            if(tabletransfer.main_services == "House Bill"){
                frm.set_value("waybill",tabletransfer.house_bill);
            }
                frm.set_value("waybill",tabletransfer.house_bill);
                frm.set_value("delivery_place",tabletransfer.delivery_port);
                frm.set_value("shipper",tabletransfer.shipper);
                frm.set_value("shipper_name",tabletransfer.shipper_name);
                frm.set_value("vessel_name",tabletransfer.vessel_name);
                frm.set_value("pod",tabletransfer.pod);
                frm.set_value("main_service",tabletransfer.main_services);
                frm.set_value("customer",tabletransfer.customer);
                frm.set_value("customer_name",tabletransfer.customer_name);
                frm.set_value("company",tabletransfer.company);
        });
    }
  }
  });