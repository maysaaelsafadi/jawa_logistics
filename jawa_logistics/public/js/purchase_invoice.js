frappe.provide("erpnext.accounts");

frappe.ui.form.on('Purchase Invoice', {
    // onload: function(frm) {
    //     if (frm.doc.docstatus === 0) {
    //         set_timeout(frm);
    //         // frm.clear_custom_buttons();
    //     }
    // },
    // refresh: function(frm) {
    //     if (frm.doc.docstatus === 0) {
    //         set_timeout(frm);
    //         // frm.clear_custom_buttons();
    //     }
    // },
    // onload_post_render: function(frm) {
    //     if (frm.doc.docstatus === 0) {
    //         set_timeout(frm);
    //         // frm.clear_custom_buttons();
    //     }    
    // },
    setup: function(frm) {
        frm.set_query("file_info", function () {
            return {
                filters: {
                    docstatus: 1
                },
            };
        });
    },
    file_info: function(frm) {
        fetch_supplier(frm)
    },
    payee_type: function(frm) {
        fetch_supplier(frm)
    }
})

frappe.ui.form.on('Purchase Invoice Item', {
    // item_code: function (frm, cdt, cdn) {
    //     var d = frappe.model.get_doc(cdt, cdn);
    //     if (d.item_code) {
    //         frappe.call({
    //             method: "jawa_logistics.get_item_tax_template",
    //             args: {
    //                 item_code: d.item_code,
    //                 is_sales: 0
    //             },
    //             callback: function(r) {
    //                 if(r.message) {
    //                     console.log(r.message)
    //                     frappe.model.set_value(cdt, cdn, 'item_tax_template', r.message)
    //                 }
    //             }
    //         });
    //     }
    // },
    item_tax_template_c: function (frm, cdt, cdn) {
		var row = locals[cdt][cdn];
		frappe.model.set_value(cdt, cdn, 'item_tax_template', row.item_tax_template_c);
	},
    service_start_date: function (frm, cdt, cdn) {
        set_end_month(frm, cdt, cdn)
    },
    no_of_months: function (frm, cdt, cdn) {
        set_end_month(frm, cdt, cdn)
    },
    
})

var fetch_supplier = function(frm) {
    if (frm.doc.file_info && frm.doc.payee_type) {
        console.log("im here!")
        if (frm.doc.payee_type == 'Agent') {
            frappe.db.get_value("File Info", frm.doc.file_info, "agent", (r) => {
                // console.log(r.agent)
                frm.set_value('supplier', r.agent)
            })
        } else if (frm.doc.payee_type == 'Shipper') {
            frappe.db.get_value("File Info", frm.doc.file_info, "shipper", (r) => {
                frm.set_value('supplier', r.shipper)
            })
        } else if (frm.doc.payee_type == 'Supplier') {
            frappe.db.get_value("File Info", frm.doc.file_info, "supplier", (r) => {
                frm.set_value('supplier', r.supplier)
            })           
        }
    }
}

var set_timeout = function(frm) {
    setTimeout(() => {
            frm.remove_custom_button("Purchase Order", 'Get items from');
            frm.remove_custom_button("Purchase Receipt", 'Get items from');
            frm.remove_custom_button("Payment Request", 'Create'); 
        }, 500);
}


var set_end_month = function(frm, cdt, cdn) {
    var d = frappe.model.get_doc(cdt, cdn);
    if (d.no_of_months && d.service_start_date) {
        var start_date = new Date(d.service_start_date);
        var end_date = new Date(start_date.setMonth(start_date.getMonth() + d.no_of_months));
        frappe.model.set_value(cdt, cdn, "service_end_date", end_date);
    }
}

cur_frm.set_query("item_tax_template_c", "items", function (doc, cdt, cdn) {
	var d = locals[cdt][cdn];
	return {
		filters: [
			['Item Tax Template', 'is_sales', '=', 0]
		]
	};
});

// erpnext.accounts.PurchaseInvoice = erpnext.buying.BuyingController.extend({
// 	setup: function(doc) {
// 		this.setup_posting_date_time_check();
// 		this._super(doc);
//
// 		// formatter for purchase invoice item
// 		if(this.frm.doc.update_stock) {
// 			this.frm.set_indicator_formatter('item_code', function(doc) {
// 				return (doc.qty<=doc.received_qty) ? "green" : "orange";
// 			});
// 		}
// 	},
// 	onload: function() {
// 		this._super();
//
// 		if(!this.frm.doc.__islocal) {
// 			// show credit_to in print format
// 			if(!this.frm.doc.supplier && this.frm.doc.credit_to) {
// 				this.frm.set_df_property("credit_to", "print_hide", 0);
// 			}
// 		}
// 	},
//
// 	refresh: function(doc) {
// 		const me = this;
// 		this._super();
//
// 		hide_fields(this.frm.doc);
// 		// Show / Hide button
// 		this.show_general_ledger();
//
// 		if(doc.update_stock==1 && doc.docstatus==1) {
// 			this.show_stock_ledger();
// 		}
//
// 		if(!doc.is_return && doc.docstatus == 1 && doc.outstanding_amount != 0){
// 			if(doc.on_hold) {
// 				this.frm.add_custom_button(
// 					__('Change Release Date'),
// 					function() {me.change_release_date()},
// 					__('Hold Invoice')
// 				);
// 				this.frm.add_custom_button(
// 					__('Unblock Invoice'),
// 					function() {me.unblock_invoice()},
// 					__('Create')
// 				);
// 			} else if (!doc.on_hold) {
// 				this.frm.add_custom_button(
// 					__('Block Invoice'),
// 					function() {me.block_invoice()},
// 					__('Create')
// 				);
// 			}
// 		}
//
// 		if(doc.docstatus == 1 && doc.outstanding_amount != 0
// 			&& !(doc.is_return && doc.return_against)) {
// 			this.frm.add_custom_button(__('Payment'), this.make_payment_entry, __('Create'));
// 			cur_frm.page.set_inner_btn_group_as_primary(__('Create'));
// 		}
//
// 		if(!doc.is_return && doc.docstatus==1) {
// 			if(doc.outstanding_amount >= 0 || Math.abs(flt(doc.outstanding_amount)) < flt(doc.grand_total)) {
// 				cur_frm.add_custom_button(__('Return / Debit Note'),
// 					this.make_debit_note, __('Create'));
// 			}
//
// 			if(!doc.auto_repeat) {
// 				cur_frm.add_custom_button(__('Subscription'), function() {
// 					erpnext.utils.make_subscription(doc.doctype, doc.name)
// 				}, __('Create'))
// 			}
// 		}
//
// 		if (doc.outstanding_amount > 0 && !cint(doc.is_return)) {
// 			cur_frm.add_custom_button(__('Payment Request'), function() {
// 				me.make_payment_request()
// 			}, __('Create'));
// 		}
//
// 		if(doc.docstatus===0) {
// 			this.frm.add_custom_button(__('Purchase Order'), function() {
// 				erpnext.utils.map_current_doc({
// 					method: "erpnext.buying.doctype.purchase_order.purchase_order.make_purchase_invoice",
// 					source_doctype: "Purchase Order",
// 					target: me.frm,
// 					setters: {
// 						supplier: me.frm.doc.supplier || undefined,
// 					},
// 					get_query_filters: {
// 						docstatus: 1,
// 						status: ["not in", ["Closed", "On Hold"]],
// 						per_billed: ["<", 99.99],
// 						company: me.frm.doc.company
// 					}
// 				})
// 			}, __("Get items from"));
//
// 			this.frm.add_custom_button(__('Purchase Receipt'), function() {
// 				erpnext.utils.map_current_doc({
// 					method: "erpnext.stock.doctype.purchase_receipt.purchase_receipt.make_purchase_invoice",
// 					source_doctype: "Purchase Receipt",
// 					target: me.frm,
// 					date_field: "posting_date",
// 					setters: {
// 						supplier: me.frm.doc.supplier || undefined,
// 					},
// 					get_query_filters: {
// 						docstatus: 1,
// 						status: ["not in", ["Closed", "Completed"]],
// 						company: me.frm.doc.company,
// 						is_return: 0
// 					}
// 				})
// 			}, __("Get items from"));
// 		}
// 		this.frm.toggle_reqd("supplier_warehouse", this.frm.doc.is_subcontracted==="Yes");
//
// 		if (doc.docstatus == 1 && !doc.inter_company_invoice_reference) {
// 			frappe.model.with_doc("Supplier", me.frm.doc.supplier, function() {
// 				var supplier = frappe.model.get_doc("Supplier", me.frm.doc.supplier);
// 				var internal = supplier.is_internal_supplier;
// 				var disabled = supplier.disabled;
// 				if (internal == 1 && disabled == 0) {
// 					me.frm.add_custom_button("Inter Company Invoice", function() {
// 						me.make_inter_company_invoice(me.frm);
// 					}, __('Create'));
// 				}
// 			});
// 		}
// 	},
//
// 	unblock_invoice: function() {
// 		const me = this;
// 		frappe.call({
// 			'method': 'erpnext.accounts.doctype.purchase_invoice.purchase_invoice.unblock_invoice',
// 			'args': {'name': me.frm.doc.name},
// 			'callback': (r) => me.frm.reload_doc()
// 		});
// 	},
//
// 	block_invoice: function() {
// 		this.make_comment_dialog_and_block_invoice();
// 	},
//
// 	change_release_date: function() {
// 		this.make_dialog_and_set_release_date();
// 	},
//
// 	can_change_release_date: function(date) {
// 		const diff = frappe.datetime.get_diff(date, frappe.datetime.nowdate());
// 		if (diff < 0) {
// 			frappe.throw(__('New release date should be in the future'));
// 			return false;
// 		} else {
// 			return true;
// 		}
// 	},
//
// 	make_comment_dialog_and_block_invoice: function(){
// 		const me = this;
//
// 		const title = __('Block Invoice');
// 		const fields = [
// 			{
// 				fieldname: 'release_date',
// 				read_only: 0,
// 				fieldtype:'Date',
// 				label: __('Release Date'),
// 				default: me.frm.doc.release_date,
// 				reqd: 1
// 			},
// 			{
// 				fieldname: 'hold_comment',
// 				read_only: 0,
// 				fieldtype:'Small Text',
// 				label: __('Reason For Putting On Hold'),
// 				default: ""
// 			},
// 		];
//
// 		this.dialog = new frappe.ui.Dialog({
// 			title: title,
// 			fields: fields
// 		});
//
// 		this.dialog.set_primary_action(__('Save'), function() {
// 			const dialog_data = me.dialog.get_values();
// 			frappe.call({
// 				'method': 'erpnext.accounts.doctype.purchase_invoice.purchase_invoice.block_invoice',
// 				'args': {
// 					'name': me.frm.doc.name,
// 					'hold_comment': dialog_data.hold_comment,
// 					'release_date': dialog_data.release_date
// 				},
// 				'callback': (r) => me.frm.reload_doc()
// 			});
// 			me.dialog.hide();
// 		});
//
// 		this.dialog.show();
// 	},
//
// 	make_dialog_and_set_release_date: function() {
// 		const me = this;
//
// 		const title = __('Set New Release Date');
// 		const fields = [
// 			{
// 				fieldname: 'release_date',
// 				read_only: 0,
// 				fieldtype:'Date',
// 				label: __('Release Date'),
// 				default: me.frm.doc.release_date
// 			},
// 		];
//
// 		this.dialog = new frappe.ui.Dialog({
// 			title: title,
// 			fields: fields
// 		});
//
// 		this.dialog.set_primary_action(__('Save'), function() {
// 			me.dialog_data = me.dialog.get_values();
// 			if(me.can_change_release_date(me.dialog_data.release_date)) {
// 				me.dialog_data.name = me.frm.doc.name;
// 				me.set_release_date(me.dialog_data);
// 				me.dialog.hide();
// 			}
// 		});
//
// 		this.dialog.show();
// 	},
//
// 	set_release_date: function(data) {
// 		return frappe.call({
// 			'method': 'erpnext.accounts.doctype.purchase_invoice.purchase_invoice.change_release_date',
// 			'args': data,
// 			'callback': (r) => this.frm.reload_doc()
// 		});
// 	},
//
// 	supplier: function() {
// 		var me = this;
// 		if(this.frm.updating_party_details)
// 			return;
// 		erpnext.utils.get_party_details(this.frm, "erpnext.accounts.party.get_party_details",
// 			{
// 				posting_date: this.frm.doc.posting_date,
// 				bill_date: this.frm.doc.bill_date,
// 				party: this.frm.doc.supplier,
// 				party_type: "Supplier",
// 				account: this.frm.doc.credit_to,
// 				price_list: this.frm.doc.buying_price_list
// 			}, function() {
// 				me.apply_pricing_rule();
// 				me.frm.doc.apply_tds = me.frm.supplier_tds ? 1 : 0;
// 				me.frm.doc.tax_withholding_category = me.frm.supplier_tds;
// 				me.frm.set_df_property("apply_tds", "read_only", me.frm.supplier_tds ? 0 : 1);
// 				me.frm.set_df_property("tax_withholding_category", "hidden", me.frm.supplier_tds ? 0 : 1);
// 			})
// 	},
//
// 	apply_tds: function(frm) {
// 		var me = this;
//
// 		if (!me.frm.doc.apply_tds) {
// 			me.frm.set_value("tax_withholding_category", '');
// 			me.frm.set_df_property("tax_withholding_category", "hidden", 1);
// 		} else {
// 			me.frm.set_value("tax_withholding_category", me.frm.supplier_tds);
// 			me.frm.set_df_property("tax_withholding_category", "hidden", 0);
// 		}
// 	},
//
// 	credit_to: function() {
// 		var me = this;
// 		if(this.frm.doc.credit_to) {
// 			me.frm.call({
// 				method: "frappe.client.get_value",
// 				args: {
// 					doctype: "Account",
// 					fieldname: "account_currency",
// 					filters: { name: me.frm.doc.credit_to },
// 				},
// 				callback: function(r, rt) {
// 					if(r.message) {
// 						me.frm.set_value("party_account_currency", r.message.account_currency);
// 						me.set_dynamic_labels();
// 					}
// 				}
// 			});
// 		}
// 	},
//
// 	make_inter_company_invoice: function(frm) {
// 		frappe.model.open_mapped_doc({
// 			method: "erpnext.accounts.doctype.purchase_invoice.purchase_invoice.make_inter_company_sales_invoice",
// 			frm: frm
// 		});
// 	},
//
// 	is_paid: function() {
// 		hide_fields(this.frm.doc);
// 		if(cint(this.frm.doc.is_paid)) {
// 			this.frm.set_value("allocate_advances_automatically", 0);
// 			if(!this.frm.doc.company) {
// 				this.frm.set_value("is_paid", 0)
// 				frappe.msgprint(__("Please specify Company to proceed"));
// 			}
// 		}
// 		this.calculate_outstanding_amount();
// 		this.frm.refresh_fields();
// 	},
//
// 	write_off_amount: function() {
// 		this.set_in_company_currency(this.frm.doc, ["write_off_amount"]);
// 		this.calculate_outstanding_amount();
// 		this.frm.refresh_fields();
// 	},
//
// 	paid_amount: function() {
// 		this.set_in_company_currency(this.frm.doc, ["paid_amount"]);
// 		this.write_off_amount();
// 		this.frm.refresh_fields();
// 	},
//
// 	allocated_amount: function() {
// 		this.calculate_total_advance();
// 		this.frm.refresh_fields();
// 	},
//
// 	items_add: function(doc, cdt, cdn) {
// 		var row = frappe.get_doc(cdt, cdn);
// 		this.frm.script_manager.copy_from_first_row("items", row,
// 			["expense_account", "cost_center", "project"]);
// 	},
//
// 	on_submit: function() {
// 		$.each(this.frm.doc["items"] || [], function(i, row) {
// 			if(row.purchase_receipt) frappe.model.clear_doc("Purchase Receipt", row.purchase_receipt)
// 		})
// 	},
//
// 	make_debit_note: function() {
// 		frappe.model.open_mapped_doc({
// 			method: "erpnext.accounts.doctype.purchase_invoice.purchase_invoice.make_debit_note",
// 			frm: cur_frm
// 		})
// 	},
//
//     validate_company_and_party: function() {
//         var me = this;
// 		var valid = true;
// 		$.each(["company", "customer"], function(i, fieldname) {
// 			if(frappe.meta.has_field(me.frm.doc.doctype, fieldname) && me.frm.doc.doctype != "Purchase Order") {
// 				if (!me.frm.doc[fieldname]) {
// 					if(me.frm.doc.doctype == "Purchase Invoice" && (frappe.meta.has_field(me.frm.doc.doctype, "file_info") && !me.frm.doc.file_info)){
// 						return valid
// 					}
// 					frappe.msgprint(__("Please specify") + ": " +
// 						frappe.meta.get_label(me.frm.doc.doctype, fieldname, me.frm.doc.name) +
// 						". " + __("It is needed to fetch Item Details."));
// 					valid = false;
// 				}
// 			}
// 		});
// 		return valid;
// 	}
//
// });
// // for backward compatibility: combine new and previous states
// $.extend(cur_frm.cscript, new erpnext.accounts.PurchaseInvoice({frm: cur_frm}));
