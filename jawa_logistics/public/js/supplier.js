frappe.ui.form.on('Supplier', {
	supplier_primary_address: function(frm) {
        erpnext.utils.get_address_display(frm, 'supplier_primary_address', 'primary_address');
	},
	type: function(frm) {
		if (frm.doc.type == 'Agent') {
		    frm.set_value('naming_series', '1')
		} else if (frm.doc.type == 'Supplier') {
		    frm.set_value('naming_series', '4')
		} else if (frm.doc.type == 'Shipper') {
		    frm.set_value('naming_series', '5')
		}
	}
})