# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
      {
            "label": _("Setup"),
            "items": [
                {
                    "type": "doctype",
                    "name": "Customer",
                },
                {
                    "type": "doctype",
                    "name": "Supplier",
                },
                {
                    "type": "doctype",
                    "name": "Address",
                },
                {
                    "type": "doctype",
                    "name": "Contact",
                },
                {
                    "type": "doctype",
                    "name": "Tracking Status",
                },
                {
                    "type": "doctype",
                    "name": "Air Line",
                },
                {
                    "type": "doctype",
                    "name": "Vessel",
                },
            ]
        },
		{
              "label": _("Transaction"),
              "items": [
                  {
                      "type": "doctype",
                      "name": "File Info",
                  },
                  {
                      "type": "doctype",
                      "name": "Delivery Note",
                  },
              ]
          },
		{
              "label": _("Accounting Transaction"),
              "items": [
                  {
                      "type": "doctype",
                      "name": "Sales Invoice",
                  },
                  {
                      "type": "doctype",
                      "name": "Purchase Invoice",
                  },
                  {
                      "type": "doctype",
                      "name": "Payment Request",
                  },
              ]
        },

		{
              "label": _("Reports"),
              "items": [
                  {
                      "type": "report",
                       "is_query_report": True,
                      "name": "Profit and Loss Statement",
                  },
                  {
                      "type": "report",
                       "is_query_report": True,
                      "name": "Statement of Account",
                  },
                  {
                      "type": "report",
                       "is_query_report": True,
                      "name": "Daily Report",
                  },
                  {
                      "type": "report",
                       "is_query_report": True,
                      "name": "Statement Report",
                  },
                  {
                      "type": "report",
                       "is_query_report": True,
                      "name": "Asset Depreciation Schedules Summary",
                  },
                  {
                      "type": "doctype",
                      "doctype": "Sales Invoice",
                      "name":"Unpaid Sales Invoice",
                      "route": "/Report/Unpaid Sales Invoices",
                  },
             
                  {
                      "type": "report",
                      "name": "Accounts Receivable Summary",
                      "doctype": "Sales Invoice",
                      "is_query_report": True
                  },
                    {
                      "type": "report",
                      "name": "Unposted Files",
                      "doctype": "File Info",
                      "is_query_report": True
                  },
                  {
					"type": "page",
					"name": "sales-dashboard",
					"label": _("Sales Dashboard")
                  }
              ]
        },
	]
