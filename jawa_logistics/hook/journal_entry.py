import frappe
from num2words import num2words

@frappe.whitelist()
def update_In_words(doc,method):	
	if doc.total_debit.is_integer() == False:
		doc.total_amount_in_words_ar = num2words(doc.total_debit, lang='ar').replace(","," ريال سعودي و ") + " هللة "
	if doc.total_debit.is_integer() == True:
		doc.total_amount_in_words_ar = num2words(doc.total_debit, lang='ar') + " ريال سعودي فقط "

	from frappe.utils import money_in_words
	doc.total_amount_in_words = money_in_words(doc.total_debit)
 
def before_submit(doc, method):
	pass
	# for item in doc.accounts:
	# 	if not item.get("cost_center"):
	# 		frappe.throw("Cost Center in required in Accounting Entries.")