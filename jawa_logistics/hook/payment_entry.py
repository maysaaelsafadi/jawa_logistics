import frappe

def validate(self, method):
	frappe.log_error(message="{0}".format(len(self.get("references"))) , title="validate")
def before_save(self, method):
	frappe.log_error(message="{0}".format(len(self.get("references"))) , title="before_save")
def before_insert(self, method):
	frappe.log_error(message="{0}".format(len(self.get("references"))) , title="before_insert")
def on_update(self, method):
	frappe.log_error(message="{0}".format(len(self.get("references"))) , title="on_update")
