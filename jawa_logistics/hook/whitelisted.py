import frappe
from frappe import _
from six import string_types
import json


@frappe.whitelist()
@frappe.read_only()
def run(report_name, filters=None, user=None, ignore_prepared_report=False, custom_columns=None):
    from frappe.desk.query_report import get_report_doc, get_prepared_report_result, generate_report_result
    report = get_report_doc(report_name)
    if not user:
        user = frappe.session.user
    if not frappe.has_permission(report.ref_doctype, "report"):
        frappe.msgprint(_("Must have report permission to access this report."),
            raise_exception=True)

    result = None

    # custom code for overriding native reports
    from jawa_logistics.reports import overrides
    overrides.main(report_name)

    if report.prepared_report and not report.disable_prepared_report and not ignore_prepared_report and not custom_columns:
        if filters:
            if isinstance(filters, string_types):
                filters = json.loads(filters)

            dn = filters.get("prepared_report_name")
            filters.pop("prepared_report_name", None)
        else:
            dn = ""
        result = get_prepared_report_result(report, filters, dn, user)
    else:
        result = generate_report_result(report, filters, user, custom_columns)

    result["add_total_row"] = report.add_total_row and not result.get('skip_total_row', False)

    return result

@frappe.whitelist(allow_guest=True)
def make_payment_request(**args):
    """Make payment request"""
    from erpnext.accounts.doctype.payment_request.payment_request import make_payment_request
    pr = make_payment_request(**args)
    if pr:
        args = frappe._dict(args)
        pr.finance_book = frappe.db.get_value(args.dt, args.dn,"finance_book")
    return pr
@frappe.whitelist()
def get_payment_entry(dt, dn, party_amount=None, bank_account=None, bank_amount=None):
    from erpnext.accounts.doctype.payment_entry.payment_entry import get_payment_entry
    pe = get_payment_entry(dt, dn, party_amount, bank_account, bank_amount)
    if pe:
        pe.finance_book = frappe.db.get_value(dt,dn,"finance_book")
        
    return pe

@frappe.whitelist()
def make_payment_entry(docname):
    doc = frappe.get_doc("Payment Request", docname)
    pe = doc.create_payment_entry(submit=False).as_dict()
    pe.finance_book = doc.get("finance_book") or ""
    return pe

@frappe.whitelist()
def get_outstanding_reference_documents(args):
    from erpnext.accounts.doctype.payment_entry.payment_entry import get_outstanding_reference_documents
    data = get_outstanding_reference_documents(args)
    temp_data = [] 
    for d in data:
        if d.voucher_type in ("Sales Invoice", "Purchase Invoice"):
            d["total_amount"] = frappe.db.get_value(d.voucher_type, d.voucher_no, "grand_total") or d.total_amount
        if d["outstanding_amount"] > 0:
            temp_data.append(d)
    return temp_data or data


@frappe.whitelist()
def get_script(report_name):
    import os
    from frappe.desk.query_report import get_report_doc
    from frappe.modules import scrub, get_module_path
    from frappe.utils import get_html_format
    from frappe.model.utils import render_include
    from frappe.translate import send_translations

    report = get_report_doc(report_name)
    module = report.module or frappe.db.get_value("DocType", report.ref_doctype, "module")
    module_path = get_module_path(module)
    report_folder = os.path.join(module_path, "report", scrub(report.name))
    script_path = os.path.join(report_folder, scrub(report.name) + ".js")
    print_path = os.path.join(report_folder, scrub(report.name) + ".html")

    script = None
    # Customized code to override js of reports
    reports_script = frappe.get_hooks().get('app_reports_js', {})
    if reports_script and reports_script.get(report_name):
        script_path = frappe.get_app_path("jawa_logistics", reports_script.get(report_name)[0])

    if os.path.exists(script_path):
        with open(script_path, "r") as f:
            script = f.read()

    html_format = get_html_format(print_path)

    if not script and report.javascript:
        script = report.javascript

    if not script:
        script = "frappe.query_reports['%s']={}" % report_name

    # load translations
    if frappe.lang != "en":
        send_translations(frappe.get_lang_dict("report", report_name))

    return {
        "script": render_include(script),
        "html_format": html_format,
        "execution_time": frappe.cache().hget('report_execution_time', report_name) or 0
    }

