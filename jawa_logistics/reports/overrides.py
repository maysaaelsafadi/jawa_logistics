import frappe

def main(report_name=""):
    # override trial balance report
    try:
        if report_name == 'Trial Balance':
            from jawa_logistics.reports import trial_balance
            trial_balance.main()

        if report_name in ["Accounts Receivable","Accounts Receivable Summary","Accounts Payable","Accounts Payable Summary"]:
            from jawa_logistics.reports import accounts_receivable
            from jawa_logistics.reports import accounts_receivable_summary
            accounts_receivable.main()
            accounts_receivable_summary.main()
    except:
        pass
