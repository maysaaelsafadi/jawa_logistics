from frappe import _
import frappe, erpnext

def _accounts_receivable():
    from frappe import _, scrub
    from frappe.utils import cint, getdate,flt
    from erpnext.accounts.report.accounts_receivable.accounts_receivable import ReceivablePayableReport

    def _set_ageing(self, row):
        if self.filters.ageing_based_on == "Due Date":
            # use posting date as a fallback for advances posted via journal and payment entry
            # when ageing viewed by due date
            entry_date = row.due_date or row.posting_date
        elif self.filters.ageing_based_on == "Supplier Invoice Date":
            entry_date = row.bill_date
        else:
            entry_date = row.posting_date

        self.get_ageing_data(entry_date, row)

        # ageing buckets should not have amounts if due date is not reached
        if getdate(entry_date) > getdate(self.filters.report_date):
            row.range1 = row.range2 = row.range3 = row.range4 = row.range5 = row.range6 = row.range7 = row.range8 = 0.0

        row.total_due = row.range1 + row.range2 + row.range3 + row.range4 + (row.range5 or 0.0) + (row.range6 or 0.0) + (row.range7 or 0.0) + (row.range8 or 0.0)

    def _get_ageing_data(self, entry_date, row):
        # [0-30, 30-60, 60-90, 90-120, 12-180 , 180-360, 360-520, 520-above]
        row.range1 = row.range2 = row.range3 = row.range4 = row.range5 = row.range6 = row.range7 = row.range8 = 0.0

        if not (self.age_as_on and entry_date):
            return

        row.age = (getdate(self.age_as_on) - getdate(entry_date)).days or 0
        index = None

        if not (
            self.filters.range1 and self.filters.range2 and self.filters.range3 and self.filters.range4 and self.filters.range5 and self.filters.range6 and self.filters.range7 
        ):
            self.filters.range1, self.filters.range2, self.filters.range3, self.filters.range4 , self.filters.range5 , self.filters.range6 , self.filters.range7 = (
                30,
                60,
                90,
                120,
                180,
                360,
                520
            )

        for i, days in enumerate(
            [self.filters.range1, self.filters.range2, self.filters.range3, self.filters.range4, self.filters.range5, self.filters.range6, self.filters.range7]
        ):
            if cint(row.age) <= cint(days):
                index = i
                break

        if index is None:
            index = 7
        row["range" + str(index + 1)] = row.outstanding
    def _get_currency_fields(self):
        return [
            "invoiced",
            "paid",
            "credit_note",
            "outstanding",
            "range1",
            "range2",
            "range3",
            "range4",
            "range5",
            "range6",
            "range7",
            "range8",
            "future_amount",
            "remaining_balance",
        ]
    def _setup_ageing_columns(self):
        # for charts
        self.ageing_column_labels = []
        self.add_column(label=_("Age (Days)"), fieldname="age", fieldtype="Int", width=80)

        for i, label in enumerate(
            [
                "0-{range1}".format(range1=self.filters["range1"]),
                "{range1}-{range2}".format(
                    range1=cint(self.filters["range1"]) + 1, range2=self.filters["range2"]
                ),
                "{range2}-{range3}".format(
                    range2=cint(self.filters["range2"]) + 1, range3=self.filters["range3"]
                ),
                "{range3}-{range4}".format(
                    range3=cint(self.filters["range3"]) + 1, range4=self.filters["range4"]
                ),
                "{range4}-{range5}".format(
                    range4=cint(self.filters["range4"]) + 1, range5=self.filters["range5"] if self.filters.get("range5") else 150
                ),
                "{range5}-{range6}".format(
                    range5=cint(self.filters["range5"])  if self.filters.get("range5") else 150 + 1, range6=self.filters["range6"] if self.filters.get("range6") else 180
                ),
                "{range6}-{range7}".format(
                    range6=cint(self.filters["range6"]) if self.filters.get("range6") else 180 + 1, range7=self.filters["range7"] if self.filters.get("range7") else 360
                ),
                "{range7}-{above}".format(range7=cint(self.filters["range7"]) if self.filters.get("range7") else 360 + 1, above=_("Above")),
            ]
        ):
            self.add_column(label=label, fieldname="range" + str(i + 1))
            self.ageing_column_labels.append(label)

    def _get_chart_data(self):
        rows = []
        for row in self.data:
            row = frappe._dict(row)
            if not cint(row.bold):
                values = [row.range1, row.range2, row.range3, row.range4, row.range5, row.range6, row.range7, row.range8,]
                precision = cint(frappe.db.get_default("float_precision")) or 2
                rows.append({"values": [flt(val, precision) for val in values]})

        self.chart = {
            "data": {"labels": self.ageing_column_labels, "datasets": rows},
            "type": "percentage",
        }


    ReceivablePayableReport.get_chart_data = _get_chart_data
    ReceivablePayableReport.set_ageing = _set_ageing
    ReceivablePayableReport.get_ageing_data = _get_ageing_data
    ReceivablePayableReport.get_currency_fields = _get_currency_fields
    ReceivablePayableReport.setup_ageing_columns = _setup_ageing_columns

def main():
    _accounts_receivable()