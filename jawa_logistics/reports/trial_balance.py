from frappe import _
import frappe, erpnext
from frappe.utils import flt, getdate, formatdate, cstr

def _trial_balance_report():
    # import erpnext.accounts.report.item_wise_purchase_register.item_wise_purchase_register as original
    import erpnext.accounts.report.trial_balance.trial_balance as original

    def fn(filters, report_type):
        from erpnext.accounts.doctype.accounting_dimension.accounting_dimension import get_accounting_dimensions, get_dimension_with_children
        additional_conditions = ""
        if not filters.show_unclosed_fy_pl_balances:
            additional_conditions = " and posting_date >= %(year_start_date)s" \
                if report_type == "Profit and Loss" else ""

        if not flt(filters.with_period_closing_entry):
            additional_conditions += " and ifnull(voucher_type, '')!='Period Closing Voucher'"

        if filters.cost_center:
            lft, rgt = frappe.db.get_value('Cost Center', filters.cost_center, ['lft', 'rgt'])
            additional_conditions += """ and cost_center in (select name from `tabCost Center`
                where lft >= %s and rgt <= %s)""" % (lft, rgt)

        if filters.project:
            additional_conditions += " and project = %(project)s"

        if filters.finance_book:
            if filters.include_default_book_entries:
                fb_conditions = " AND (finance_book in (%(finance_book)s, %(company_fb)s, '') OR finance_book IS NULL)"
            else:
                fb_conditions = " AND finance_book = %(finance_book)s"

            additional_conditions += fb_conditions

        accounting_dimensions = get_accounting_dimensions(as_list=False)

        query_filters = {
            "company": filters.company,
            "from_date": filters.from_date,
            "report_type": report_type,
            "year_start_date": filters.year_start_date,
            "project": filters.project,
            "finance_book": filters.finance_book,
            "company_fb": frappe.db.get_value("Company", filters.company, 'default_finance_book')
        }

        if accounting_dimensions:
            for dimension in accounting_dimensions:
                if filters.get(dimension.fieldname):
                    if frappe.get_cached_value('DocType', dimension.document_type, 'is_tree'):
                        filters[dimension.fieldname] = get_dimension_with_children(dimension.document_type,
                            filters.get(dimension.fieldname))
                        additional_conditions += "and {0} in %({0})s".format(dimension.fieldname)
                    else:
                        additional_conditions += "and {0} in (%({0})s)".format(dimension.fieldname)

                    query_filters.update({
                        dimension.fieldname: filters.get(dimension.fieldname)
                    })

        gle = frappe.db.sql("""
            select
                account, sum(debit) as opening_debit, sum(credit) as opening_credit
            from `tabGL Entry`
            where
                company=%(company)s
                {additional_conditions}
                and (posting_date < %(from_date)s or ifnull(is_opening, 'No') = 'Yes')
                and account in (select name from `tabAccount` where report_type=%(report_type)s)
                and is_cancelled = 0 
            group by account""".format(additional_conditions=additional_conditions), query_filters , as_dict=True)

        opening = frappe._dict()
        for d in gle:
            opening.setdefault(d.account, d)
        return opening

    def _get_date(filters):
        from erpnext.accounts.report.financial_statements import filter_accounts, filter_out_zero_value_rows
        accounts = frappe.db.sql("""select name, account_number, parent_account, account_name, root_type, report_type, lft, rgt

            from `tabAccount` where company=%s order by lft""", filters.company, as_dict=True)
        company_currency = erpnext.get_company_currency(filters.company)

        if not accounts:
            return None

        accounts, accounts_by_name, parent_children_map = filter_accounts(accounts)

        min_lft, max_rgt = frappe.db.sql("""select min(lft), max(rgt) from `tabAccount`
            where company=%s""", (filters.company,))[0]

        gl_entries_by_account = {}

        opening_balances = original.get_opening_balances(filters)

        #add filter inside list so that the query in financial_statements.py doesn't break
        if filters.project:
            filters.project = [filters.project]

        set_gl_entries_by_account(filters.company, filters.from_date,
            filters.to_date, min_lft, max_rgt, filters, gl_entries_by_account, ignore_closing_entries=not flt(filters.with_period_closing_entry))


        total_row = original.calculate_values(accounts, gl_entries_by_account, opening_balances, filters, company_currency)
        original.accumulate_values_into_parents(accounts, accounts_by_name)

        data = original.prepare_data(accounts, filters, total_row, parent_children_map, company_currency)
        data = filter_out_zero_value_rows(data, parent_children_map, show_zero_values=filters.get("show_zero_values"))

        return data

    original.get_data = _get_date
    original.get_rootwise_opening_balances = fn

    

def set_gl_entries_by_account(company, from_date, to_date, root_lft, root_rgt, filters, gl_entries_by_account, ignore_closing_entries=False):
    """Returns a dict like { "account": [gl entries], ... }"""
    from erpnext.accounts.report.utils import get_currency, convert_to_presentation_currency

    additional_conditions = get_additional_conditions(from_date, ignore_closing_entries, filters)

    accounts = frappe.db.sql_list("""select name from `tabAccount`
        where lft >= %s and rgt <= %s and company = %s""", (root_lft, root_rgt, company))

    if accounts:
        additional_conditions += " and account in ({})"\
            .format(", ".join([frappe.db.escape(d) for d in accounts]))

        gl_filters = {
            "company": company,
            "from_date": from_date,
            "to_date": to_date,
            "finance_book": cstr(filters.get("finance_book"))
        }

        if filters.get("include_default_book_entries"):
            gl_filters["company_fb"] = frappe.db.get_value("Company",
                company, 'default_finance_book')

        for key, value in filters.items():
            if value:
                gl_filters.update({
                    key: value
                })

        gl_entries = frappe.db.sql("""select posting_date, account, debit, credit, is_opening, fiscal_year, debit_in_account_currency, credit_in_account_currency, account_currency from `tabGL Entry`
            where company=%(company)s
            {additional_conditions}
            and posting_date <= %(to_date)s
            AND is_cancelled = 0
            order by account, posting_date""".format(additional_conditions=additional_conditions), gl_filters, as_dict=True) #nosec

        if filters and filters.get('presentation_currency'):
            convert_to_presentation_currency(gl_entries, get_currency(filters))

        for entry in gl_entries:
            gl_entries_by_account.setdefault(entry.account, []).append(entry)

        return gl_entries_by_account

def get_additional_conditions(from_date, ignore_closing_entries, filters):
    from erpnext.accounts.doctype.accounting_dimension.accounting_dimension import get_accounting_dimensions, get_dimension_with_children
    from erpnext.accounts.report.financial_statements import get_cost_centers_with_children

    additional_conditions = []

    accounting_dimensions = get_accounting_dimensions(as_list=False)

    if ignore_closing_entries:
        additional_conditions.append("ifnull(voucher_type, '')!='Period Closing Voucher'")

    if from_date:
        additional_conditions.append("posting_date >= %(from_date)s")

    if filters:
        if filters.get("project"):
            if not isinstance(filters.get("project"), list):
                filters.project = frappe.parse_json(filters.get("project"))

            additional_conditions.append("project in %(project)s")

        if filters.get("cost_center"):
            filters.cost_center = get_cost_centers_with_children(filters.cost_center)
            additional_conditions.append("cost_center in %(cost_center)s")

        if filters.get("finance_book"):
            if filters.get("include_default_book_entries"):
                additional_conditions.append("(finance_book in (%(finance_book)s, %(company_fb)s))")
            else:
                additional_conditions.append("(finance_book = %(finance_book)s)")

    if accounting_dimensions:
        for dimension in accounting_dimensions:
            if filters.get(dimension.fieldname):
                if frappe.get_cached_value('DocType', dimension.document_type, 'is_tree'):
                    filters[dimension.fieldname] = get_dimension_with_children(dimension.document_type,
                        filters.get(dimension.fieldname))
                    additional_conditions.append("{0} in %({0})s".format(dimension.fieldname))
                else:
                    additional_conditions.append("{0} in (%({0})s)".format(dimension.fieldname))

    return " and {}".format(" and ".join(additional_conditions)) if additional_conditions else ""

    
def main():
    _trial_balance_report()


