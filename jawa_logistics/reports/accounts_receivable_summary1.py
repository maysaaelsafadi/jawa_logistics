from frappe import _
import frappe, erpnext
from frappe.utils import flt, getdate, formatdate, cstr

def _accounts_receivable_summary():
    # import erpnext.accounts.report.item_wise_purchase_register.item_wise_purchase_register as original
    from erpnext.accounts.report.accounts_receivable_summary.accounts_receivable_summary import AccountsReceivableSummary
    def _run(self, args):
        self.party_type = args.get('party_type')
        self.party_naming_by = frappe.db.get_value(args.get("naming_by")[0], None, args.get("naming_by")[1])
        self.get_columns()
        self.get_data(args)

        # Chart Data
        labels = []
        values = []
        for row in self.data:
            labels.append(row.get("party"))
            values.append(row.get("outstanding"))
        chart = {'data':{'labels':labels,'datasets':[{'values':values}]},'type':'pie'}


        return self.columns, self.data, None, chart
    AccountsReceivableSummary.run = _run


    
def main():
    _accounts_receivable_summary()


