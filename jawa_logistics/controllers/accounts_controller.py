from __future__ import unicode_literals
import frappe
from frappe import _, throw
from frappe.utils import cint, cstr, flt, formatdate, get_link_to_form, getdate, nowdate
from erpnext.accounts.utils import get_fiscal_years, get_account_currency
from erpnext.accounts.doctype.accounting_dimension.accounting_dimension import get_accounting_dimensions
import erpnext

from erpnext.controllers.accounts_controller import set_balance_in_account_currency
from erpnext.accounts.doctype.sales_invoice.sales_invoice import SalesInvoice
from erpnext.accounts.doctype.purchase_invoice.purchase_invoice import PurchaseInvoice
from erpnext.accounts.doctype.sales_invoice.sales_invoice import set_account_for_mode_of_payment
from erpnext.stock import get_warehouse_account_map
from erpnext.assets.doctype.asset.asset import get_asset_account, is_cwip_accounting_enabled
from erpnext.assets.doctype.asset_category.asset_category import get_asset_category_account
from erpnext.controllers.accounts_controller import validate_account_head
from six import iteritems

from erpnext.stock.doctype.purchase_receipt.purchase_receipt import get_item_account_wise_additional_cost



class CustomSalesInvoice(SalesInvoice):

    def get_gl_dict(self, args, account_currency=None, item=None):
        """this method populates the common properties of a gl entry record"""
        posting_date = args.get('posting_date') or self.get('posting_date')
        fiscal_years = get_fiscal_years(posting_date, company=self.company)
        if len(fiscal_years) > 1:
            frappe.throw(_("Multiple fiscal years exist for the date {0}. Please set company in Fiscal Year").format(
                formatdate(posting_date)))
        else:
            fiscal_year = fiscal_years[0][0]

        gl_dict = frappe._dict({
            'company': self.company,
            'posting_date': posting_date,
            'fiscal_year': fiscal_year,
            'voucher_type': self.doctype,
            'voucher_no': self.name,
            'remarks': self.get("remarks") or self.get("remark"),
            'debit': 0,
            'credit': 0,
            'debit_in_account_currency': 0,
            'credit_in_account_currency': 0,
            'is_opening': self.get("is_opening") or "No",
            'party_type': None,
            'party': None,
            'project': self.get("project")
        })

        accounting_dimensions = get_accounting_dimensions()
        dimension_dict = frappe._dict()

        for dimension in accounting_dimensions:
            dimension_dict[dimension] = self.get(dimension)
            if item and item.get(dimension):
                dimension_dict[dimension] = item.get(dimension)

        gl_dict.update(dimension_dict)
        gl_dict.update(args)
        if item and item.get("finance_book"):
            gl_dict.update({"finance_book":item.get("finance_book")})
        if not account_currency:
            account_currency = get_account_currency(gl_dict.account)

        if gl_dict.account and self.doctype not in ["Journal Entry",
                                                    "Period Closing Voucher", "Payment Entry"]:
            self.validate_account_currency(gl_dict.account, account_currency)
            set_balance_in_account_currency(gl_dict, account_currency, self.get("conversion_rate"),
                                            self.company_currency)


        #JLO-122
        #loop in all purchase invoice that connected to that file with work_in_progress=1
        # Commment below code because this should by triggered on submit hook instead of get_gl_dict method
        # if self.file_info:
        #    self.create_purchase_invoice_gl()

        return gl_dict

    def before_save(self):

        from num2words import num2words
        if float(self.grand_total).is_integer() == False:
            self.in_words_ar = num2words(self.grand_total, lang='ar').replace(",", " ريال سعودي و ") + " هللة "

        if float(self.grand_total).is_integer() == True:
            self.in_words_ar = num2words(self.grand_total, lang='ar') + " ريال سعودي فقط "
        array_list = ["مائة", "مئتان", "ثلاثمائة", "أربعمائة", "خمسمائة", "ستمائة", "سبعمائة", "ثمانمائة", "تسعمائة"]
        repStr = self.in_words_ar
        for i in array_list:
            repStr = repStr.replace(i, '{0} و '.format(i))
            repStr = repStr.replace(' و  و ', ' و ')
            repStr = repStr.replace('و  ألفاً', 'ألف')
        self.in_words_ar = repStr


        set_account_for_mode_of_payment(self)

    def before_update_after_submit(self):
        from num2words import num2words
        if float(self.grand_total).is_integer() == False:
            self.in_words_ar = num2words(self.grand_total, lang='ar').replace(",", " ريال سعودي و ") + " هللة "

        if float(self.grand_total).is_integer() == True:
            self.in_words_ar = num2words(self.grand_total, lang='ar') + " ريال سعودي فقط "
        array_list = ["مائة", "مئتان", "ثلاثمائة", "أربعمائة", "خمسمائة", "ستمائة", "سبعمائة", "ثمانمائة", "تسعمائة"]
        repStr = self.in_words_ar
        for i in array_list:
            repStr = repStr.replace(i, '{0} و '.format(i))
            repStr = repStr.replace(' و  و ', ' و ')
            repStr = repStr.replace('و  ألفاً', 'ألف')
        self.in_words_ar = repStr


class CustomPurchaseInvoice(PurchaseInvoice):
    def get_gl_dict(self, args, account_currency=None, item=None):
        """this method populates the common properties of a gl entry record"""
        posting_date = args.get('posting_date') or self.get('posting_date')
        fiscal_years = get_fiscal_years(posting_date, company=self.company)
        if len(fiscal_years) > 1:
            frappe.throw(_("Multiple fiscal years exist for the date {0}. Please set company in Fiscal Year").format(
                formatdate(posting_date)))
        else:
            fiscal_year = fiscal_years[0][0]

        gl_dict = frappe._dict({
            'company': self.company,
            'posting_date': posting_date,
            'fiscal_year': fiscal_year,
            'voucher_type': self.doctype,
            'voucher_no': self.name,
            'remarks': self.get("remarks") or self.get("remark"),
            'debit': 0,
            'credit': 0,
            'debit_in_account_currency': 0,
            'credit_in_account_currency': 0,
            'is_opening': self.get("is_opening") or "No",
            'party_type': None,
            'party': None,
            'project': self.get("project")
        })

        accounting_dimensions = get_accounting_dimensions()
        dimension_dict = frappe._dict()

        for dimension in accounting_dimensions:
            dimension_dict[dimension] = self.get(dimension)
            if item and item.get(dimension):
                dimension_dict[dimension] = item.get(dimension)

        gl_dict.update(dimension_dict)
        gl_dict.update(args)
        if item and item.get("finance_book"):
            gl_dict.update({"finance_book":item.get("finance_book")})
        if not account_currency:
            account_currency = get_account_currency(gl_dict.account)

        if gl_dict.account and self.doctype not in ["Journal Entry",
                                                    "Period Closing Voucher", "Payment Entry"]:
            self.validate_account_currency(gl_dict.account, account_currency)
            set_balance_in_account_currency(gl_dict, account_currency, self.get("conversion_rate"),
                                            self.company_currency)

        return gl_dict


    def make_item_gl_entries(self, gl_entries):
        # item gl entries
        stock_items = self.get_stock_items()
        if self.update_stock and self.auto_accounting_for_stock:
            warehouse_account = get_warehouse_account_map(self.company)

        landed_cost_entries = get_item_account_wise_additional_cost(self.name)

        voucher_wise_stock_value = {}
        if self.update_stock:
            stock_ledger_entries = frappe.get_all(
                "Stock Ledger Entry",
                fields=["voucher_detail_no", "stock_value_difference", "warehouse"],
                filters={"voucher_no": self.name, "voucher_type": self.doctype, "is_cancelled": 0},
            )
            for d in stock_ledger_entries:
                voucher_wise_stock_value.setdefault(
                    (d.voucher_detail_no, d.warehouse), d.stock_value_difference
                )

        valuation_tax_accounts = [
            d.account_head
            for d in self.get("taxes")
            if d.category in ("Valuation", "Total and Valuation")
               and flt(d.base_tax_amount_after_discount_amount)
        ]

        provisional_accounting_for_non_stock_items = cint(
            frappe.db.get_value(
                "Company", self.company, "enable_provisional_accounting_for_non_stock_items"
            )
        )

        purchase_receipt_doc_map = {}

        for item in self.get("items"):
            original_expense_account = item.expense_account
            if self.file_info and self.work_in_progress==1:
                wip_account=frappe.db.get_value('Account', { 'wip_account': 1},'name')
                if not wip_account:
                    frappe.throw("Please set one account as WIP Account")
                original_expense_account=wip_account



            if flt(item.base_net_amount):
                account_currency = get_account_currency(original_expense_account)
                if item.item_code:
                    asset_category = frappe.get_cached_value("Item", item.item_code, "asset_category")

                if self.update_stock and self.auto_accounting_for_stock and item.item_code in stock_items:
                    # warehouse account
                    warehouse_debit_amount = self.make_stock_adjustment_entry(
                        gl_entries, item, voucher_wise_stock_value, account_currency
                    )

                    if item.from_warehouse:
                        gl_entries.append(
                            self.get_gl_dict(
                                {
                                    "account": warehouse_account[item.warehouse]["account"],
                                    "against": warehouse_account[item.from_warehouse]["account"],
                                    "cost_center": item.cost_center,
                                    "project": item.project or self.project,
                                    "remarks": self.get("remarks") or _("Accounting Entry for Stock"),
                                    "debit": warehouse_debit_amount,
                                },
                                warehouse_account[item.warehouse]["account_currency"],
                                item=item,
                            )
                        )

                        credit_amount = item.base_net_amount
                        if self.is_internal_supplier and item.valuation_rate:
                            credit_amount = flt(item.valuation_rate * item.stock_qty)

                        # Intentionally passed negative debit amount to avoid incorrect GL Entry validation
                        gl_entries.append(
                            self.get_gl_dict(
                                {
                                    "account": warehouse_account[item.from_warehouse]["account"],
                                    "against": warehouse_account[item.warehouse]["account"],
                                    "cost_center": item.cost_center,
                                    "project": item.project or self.project,
                                    "remarks": self.get("remarks") or _("Accounting Entry for Stock"),
                                    "debit": -1 * flt(credit_amount, item.precision("base_net_amount")),
                                },
                                warehouse_account[item.from_warehouse]["account_currency"],
                                item=item,
                            )
                        )

                        # Do not book expense for transfer within same company transfer
                        if not self.is_internal_transfer():
                            gl_entries.append(
                                self.get_gl_dict(
                                    {
                                        "account":original_expense_account,
                                        "against": self.supplier,
                                        "debit": flt(item.base_net_amount, item.precision("base_net_amount")),
                                        "remarks": self.get("remarks") or _("Accounting Entry for Stock"),
                                        "cost_center": item.cost_center,
                                        "project": item.project,
                                    },
                                    account_currency,
                                    item=item,
                                )
                            )

                    else:
                        if not self.is_internal_transfer():
                            gl_entries.append(
                                self.get_gl_dict(
                                    {
                                        "account": original_expense_account,
                                        "against": self.supplier,
                                        "debit": warehouse_debit_amount,
                                        "remarks": self.get("remarks") or _("Accounting Entry for Stock"),
                                        "cost_center": item.cost_center,
                                        "project": item.project or self.project,
                                    },
                                    account_currency,
                                    item=item,
                                )
                            )

                    # Amount added through landed-cost-voucher
                    if landed_cost_entries:
                        for account, amount in iteritems(landed_cost_entries[(item.item_code, item.name)]):
                            gl_entries.append(
                                self.get_gl_dict(
                                    {
                                        "account": account,
                                        "against": original_expense_account,
                                        "cost_center": item.cost_center,
                                        "remarks": self.get("remarks") or _("Accounting Entry for Stock"),
                                        "credit": flt(amount["base_amount"]),
                                        "credit_in_account_currency": flt(amount["amount"]),
                                        "project": item.project or self.project,
                                    },
                                    item=item,
                                )
                            )

                    # sub-contracting warehouse
                    if flt(item.rm_supp_cost):
                        supplier_warehouse_account = warehouse_account[self.supplier_warehouse]["account"]
                        if not supplier_warehouse_account:
                            frappe.throw(_("Please set account in Warehouse {0}").format(self.supplier_warehouse))
                        gl_entries.append(
                            self.get_gl_dict(
                                {
                                    "account": supplier_warehouse_account,
                                    "against": original_expense_account,
                                    "cost_center": item.cost_center,
                                    "project": item.project or self.project,
                                    "remarks": self.get("remarks") or _("Accounting Entry for Stock"),
                                    "credit": flt(item.rm_supp_cost),
                                },
                                warehouse_account[self.supplier_warehouse]["account_currency"],
                                item=item,
                            )
                        )

                elif not item.is_fixed_asset or (
                        item.is_fixed_asset and not is_cwip_accounting_enabled(asset_category)
                ):
                    expense_account = (
                        original_expense_account
                        if (not item.enable_deferred_expense or self.is_return)
                        else item.deferred_expense_account
                    )

                    if not item.is_fixed_asset:
                        dummy, amount = self.get_amount_and_base_amount(item, None)
                    else:
                        amount = flt(item.base_net_amount + item.item_tax_amount, item.precision("base_net_amount"))

                    if provisional_accounting_for_non_stock_items:
                        if item.purchase_receipt:
                            provisional_account = frappe.db.get_value(
                                "Purchase Receipt Item", item.pr_detail, "provisional_expense_account"
                            ) or self.get_company_default("default_provisional_account")
                            purchase_receipt_doc = purchase_receipt_doc_map.get(item.purchase_receipt)

                            if not purchase_receipt_doc:
                                purchase_receipt_doc = frappe.get_doc("Purchase Receipt", item.purchase_receipt)
                                purchase_receipt_doc_map[item.purchase_receipt] = purchase_receipt_doc

                            # Post reverse entry for Stock-Received-But-Not-Billed if it is booked in Purchase Receipt
                            expense_booked_in_pr = frappe.db.get_value(
                                "GL Entry",
                                {
                                    "is_cancelled": 0,
                                    "voucher_type": "Purchase Receipt",
                                    "voucher_no": item.purchase_receipt,
                                    "voucher_detail_no": item.pr_detail,
                                    "account": provisional_account,
                                },
                                ["name"],
                            )

                            if expense_booked_in_pr:
                                # Intentionally passing purchase invoice item to handle partial billing
                                purchase_receipt_doc.add_provisional_gl_entry(
                                    item, gl_entries, self.posting_date, provisional_account, reverse=1
                                )

                    if not self.is_internal_transfer():
                        gl_entries.append(
                            self.get_gl_dict(
                                {
                                    "account": expense_account,
                                    "against": self.supplier,
                                    "debit": amount,
                                    "cost_center": item.cost_center,
                                    "project": item.project or self.project,
                                },
                                account_currency,
                                item=item,
                            )
                        )

                    # If asset is bought through this document and not linked to PR
                    if self.update_stock and item.landed_cost_voucher_amount:
                        expenses_included_in_asset_valuation = self.get_company_default(
                            "expenses_included_in_asset_valuation"
                        )
                        # Amount added through landed-cost-voucher
                        gl_entries.append(
                            self.get_gl_dict(
                                {
                                    "account": expenses_included_in_asset_valuation,
                                    "against": expense_account,
                                    "cost_center": item.cost_center,
                                    "remarks": self.get("remarks") or _("Accounting Entry for Stock"),
                                    "credit": flt(item.landed_cost_voucher_amount),
                                    "project": item.project or self.project,
                                },
                                item=item,
                            )
                        )

                        gl_entries.append(
                            self.get_gl_dict(
                                {
                                    "account": expense_account,
                                    "against": expenses_included_in_asset_valuation,
                                    "cost_center": item.cost_center,
                                    "remarks": self.get("remarks") or _("Accounting Entry for Stock"),
                                    "debit": flt(item.landed_cost_voucher_amount),
                                    "project": item.project or self.project,
                                },
                                item=item,
                            )
                        )

                        # update gross amount of asset bought through this document
                        assets = frappe.db.get_all(
                            "Asset", filters={"purchase_invoice": self.name, "item_code": item.item_code}
                        )
                        for asset in assets:
                            frappe.db.set_value("Asset", asset.name, "gross_purchase_amount", flt(item.valuation_rate))
                            frappe.db.set_value(
                                "Asset", asset.name, "purchase_receipt_amount", flt(item.valuation_rate)
                            )

            if (
                    self.auto_accounting_for_stock
                    and self.is_opening == "No"
                    and item.item_code in stock_items
                    and item.item_tax_amount
            ):
                # Post reverse entry for Stock-Received-But-Not-Billed if it is booked in Purchase Receipt
                if item.purchase_receipt and valuation_tax_accounts:
                    negative_expense_booked_in_pr = frappe.db.sql(
                        """select name from `tabGL Entry`
                            where voucher_type='Purchase Receipt' and voucher_no=%s and account in %s""",
                        (item.purchase_receipt, valuation_tax_accounts),
                    )

                    if not negative_expense_booked_in_pr:
                        gl_entries.append(
                            self.get_gl_dict(
                                {
                                    "account": self.stock_received_but_not_billed,
                                    "against": self.supplier,
                                    "debit": flt(item.item_tax_amount, item.precision("item_tax_amount")),
                                    "remarks": self.remarks or _("Accounting Entry for Stock"),
                                    "cost_center": self.cost_center,
                                    "project": item.project or self.project,
                                },
                                item=item,
                            )
                        )

                        self.negative_expense_to_be_booked += flt(
                            item.item_tax_amount, item.precision("item_tax_amount")
                        )



    def set_expense_account(self, for_validate=False):
        auto_accounting_for_stock = erpnext.is_perpetual_inventory_enabled(self.company)

        if auto_accounting_for_stock:
            stock_not_billed_account = self.get_company_default("stock_received_but_not_billed")
            stock_items = self.get_stock_items()

        asset_items = [d.is_fixed_asset for d in self.items if d.is_fixed_asset]
        if len(asset_items) > 0:
            asset_received_but_not_billed = self.get_company_default("asset_received_but_not_billed")

        if self.update_stock:
            self.validate_item_code()
            self.validate_warehouse(for_validate)
            if auto_accounting_for_stock:
                warehouse_account = get_warehouse_account_map(self.company)

        for item in self.get("items"):
            # wip_account = frappe.db.get_value('Account', {'wip_account': 1}, 'name')
            # if wip_account:
            #     if item.expense_account == wip_account:
            #         continue

            # in case of auto inventory accounting,
            # expense account is always "Stock Received But Not Billed" for a stock item
            # except opening entry, drop-ship entry and fixed asset items
            if item.item_code:
                asset_category = frappe.get_cached_value("Item", item.item_code, "asset_category")

            if (
                    auto_accounting_for_stock
                    and item.item_code in stock_items
                    and self.is_opening == "No"
                    and not item.is_fixed_asset
                    and (
                    not item.po_detail
                    or not frappe.db.get_value("Purchase Order Item", item.po_detail, "delivered_by_supplier")
            )
            ):

                if self.update_stock and item.warehouse and (not item.from_warehouse):
                    if (
                            for_validate
                            and item.expense_account
                            and item.expense_account != warehouse_account[item.warehouse]["account"]
                    ):
                        msg = _(
                            "Row {0}: Expense Head changed to {1} because account {2} is not linked to warehouse {3} or it is not the default inventory account"
                        ).format(
                            item.idx,
                            frappe.bold(warehouse_account[item.warehouse]["account"]),
                            frappe.bold(item.expense_account),
                            frappe.bold(item.warehouse),
                        )
                        frappe.msgprint(msg, title=_("Expense Head Changed"))
                    item.expense_account = warehouse_account[item.warehouse]["account"]
                else:
                    # check if 'Stock Received But Not Billed' account is credited in Purchase receipt or not
                    if item.purchase_receipt:
                        negative_expense_booked_in_pr = frappe.db.sql(
                            """select name from `tabGL Entry`
                            where voucher_type='Purchase Receipt' and voucher_no=%s and account = %s""",
                            (item.purchase_receipt, stock_not_billed_account),
                        )

                        if negative_expense_booked_in_pr:
                            if (
                                    for_validate and item.expense_account and item.expense_account != stock_not_billed_account
                            ):
                                msg = _(
                                    "Row {0}: Expense Head changed to {1} because expense is booked against this account in Purchase Receipt {2}"
                                ).format(
                                    item.idx, frappe.bold(stock_not_billed_account), frappe.bold(item.purchase_receipt)
                                )
                                frappe.msgprint(msg, title=_("Expense Head Changed"))

                            item.expense_account = stock_not_billed_account
                    else:
                        # If no purchase receipt present then book expense in 'Stock Received But Not Billed'
                        # This is done in cases when Purchase Invoice is created before Purchase Receipt
                        if (
                                for_validate and item.expense_account and item.expense_account != stock_not_billed_account
                        ):
                            msg = _(
                                "Row {0}: Expense Head changed to {1} as no Purchase Receipt is created against Item {2}."
                            ).format(
                                item.idx, frappe.bold(stock_not_billed_account), frappe.bold(item.item_code)
                            )
                            msg += "<br>"
                            msg += _(
                                "This is done to handle accounting for cases when Purchase Receipt is created after Purchase Invoice"
                            )
                            frappe.msgprint(msg, title=_("Expense Head Changed"))

                        item.expense_account = stock_not_billed_account

            elif item.is_fixed_asset and not is_cwip_accounting_enabled(asset_category):
                asset_category_account = get_asset_category_account(
                    "fixed_asset_account", item=item.item_code, company=self.company
                )
                if not asset_category_account:
                    form_link = get_link_to_form("Asset Category", asset_category)
                    throw(
                        _("Please set Fixed Asset Account in {} against {}.").format(form_link, self.company),
                        title=_("Missing Account"),
                    )
                item.expense_account = asset_category_account
            elif item.is_fixed_asset and item.pr_detail:
                item.expense_account = asset_received_but_not_billed
            elif not item.expense_account and for_validate:
                throw(_("Expense account is mandatory for item {0}").format(item.item_code or item.item_name))

        # if self.file_info and self.work_in_progress==1:
        #     wip_account=frappe.db.get_value('Account', { 'wip_account': 1},'name')
        #     if not wip_account:
        #         frappe.throw("Please set one account as WIP Account")
        #
        #     for item in self.get("items"):
        #         item.original_expense_head = item.expense_account
        #         item.expense_account = wip_account



    def validate_expense_account(self):

        for item in self.get("items"):
            validate_account_head(item.idx, item.expense_account, self.company, "Expense")




