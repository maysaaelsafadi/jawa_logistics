import frappe
from frappe import _
from jawa_logistics.controllers.base_controller import BaseController
from frappe.utils import cint, cstr, date_diff, flt, formatdate, getdate, get_link_to_form, comma_or, get_fullname, add_days, nowdate, get_datetime_str
from datetime import datetime
from dateutil import relativedelta

class SellingController(BaseController):
    def validate_sales(self):
        try:
            items = self.doc.items
            for item in items:
                if(item.enable_deferred_revenue == 1):
                    self.update_gl_entries(item)

        except Exception as e:
            frappe.db.rollback()
            frappe.msgprint(str(e))

    def update_gl_entries(self, item):
        month = 0

        if item.service_start_date:
            start_date = datetime.strptime(str(item.service_start_date), '%Y-%m-%d')
            end_date = datetime.strptime(str(item.service_end_date), '%Y-%m-%d')
            in_range = relativedelta.relativedelta(end_date, start_date)
            # month = in_range.months+1
            month = item.no_of_months

        default_deferred_revenue_account = frappe.get_cached_value('Company', self.doc.company, 'default_deferred_revenue_account')

        deferred_revenue_account = item.deferred_revenue_account or default_deferred_revenue_account

        if month > 0:
            amount = item.amount/month
            for m in range(0, month):
                date = datetime.date(start_date) + relativedelta.relativedelta(months=m)

                if(deferred_revenue_account):
                    self.add_gl(deferred_revenue_account, 0, amount, item.cost_center, date)

                if(item.income_account):
                    self.add_gl(item.income_account, amount, 0, item.cost_center, date)
        

        # month = 0

        # start_date = datetime.strptime(str(item.service_start_date), '%Y-%m-%d')
        # end_date = datetime.strptime(str(item.service_end_date), '%Y-%m-%d')
        # in_range = relativedelta.relativedelta(end_date, start_date)
        # # month = in_range.months
        # month = item.no_of_months
        
        # if(month > 1):
        #     amount = item.amount/month
        #     for m in range(0, month):
        #         date = datetime.date(start_date) + relativedelta.relativedelta(months=m)

        #         if(item.deferred_revenue_account):
        #             self.add_gl(item.deferred_revenue_account, 0, amount, item.cost_center, date)

        #         if(item.income_account):
        #             self.add_gl(item.income_account, amount, 0, item.cost_center, date)
       

    def add_gl(self, account, credit, debit, cost_center, date):
        doc = frappe.new_doc("GL Entry")
        doc.posting_date = date
        doc.account = account
        doc.cost_center = cost_center
        doc.debit = flt(debit, 3)
        doc.debit_in_account_currency = flt(debit, 3)
        doc.credit = flt(credit, 3)
        doc.credit_in_account_currency = flt(credit, 3)
        doc.account_currency = self.doc.currency
        doc.against = self.doc.customer
        doc.voucher_type = "Sales Invoice"
        doc.voucher_no = self.doc.name
        doc.remarks = "No Remarks"
        doc.party_type = "Customer"
        doc.party = self.doc.customer
        doc.company = self.doc.company
        doc.customer = self.doc.customer
        doc.save(ignore_permissions=True)
        doc.submit()

    def set_total_in_words(self):
        from frappe.utils import money_in_words
        if not self.base_grand_total:
            self.base_grand_total=0.0
        if not self.base_rounded_total:
            self.base_rounded_total=0.0

        if self.meta.get_field("base_in_words"):
            base_amount = abs(
                self.base_grand_total if self.is_rounded_total_disabled() else self.base_rounded_total
            )
            self.base_in_words = money_in_words(base_amount, self.company_currency)

        if self.meta.get_field("in_words"):
            amount = abs(self.grand_total if self.is_rounded_total_disabled() else self.rounded_total)
            self.in_words = money_in_words(amount, self.currency)


def on_submit(doc, method=""):
    create_purchase_invoice_gl(doc)

def create_purchase_invoice_gl(self):
    from erpnext.accounts.general_ledger import make_gl_entries
    from erpnext.accounts.utils import get_fiscal_years, get_account_currency

    gl_map = []

    purchase_invoice_list = frappe.db.sql(""" select name from `tabPurchase Invoice` 
                                                        where file_info='{0}' and  work_in_progress=1 and docstatus=1 """.format(
        self.file_info), as_dict=1)

    wip_account = frappe.db.get_value('Account', {'wip_account': 1}, 'name')
    if not wip_account:
        frappe.throw("Please set one account as WIP Account")

    for purchase_invoice in purchase_invoice_list:
        purchase_invoice_doc = frappe.get_doc("Purchase Invoice", purchase_invoice['name'])

        fiscal_years = get_fiscal_years(purchase_invoice_doc.posting_date, company=self.company)
        if len(fiscal_years) > 1:
            frappe.throw(
                _("Multiple fiscal years exist for the date {0}. Please set company in Fiscal Year").format(formatdate(purchase_invoice_doc.posting_date)))
        else:
            fiscal_year = fiscal_years[0][0]


        for item in purchase_invoice_doc.items:
            # insert 2GL
            gl_map.append(
                self.get_gl_dict({
                    'company': self.company,
                    'posting_date': self.posting_date,
                    'fiscal_year': fiscal_year,
                    'voucher_type': 'Purchase Invoice',
                    'voucher_no': purchase_invoice_doc.name,
                    'remarks': self.get("remarks") or self.get("remark"),
                    'account': wip_account,
                    'debit': 0,
                    'credit': item.base_amount,
                    'debit_in_account_currency': 0,
                    'credit_in_account_currency': item.base_amount,
                    'is_opening': "No",
                    'party_type': None,
                    'party': None,
                    'project': self.get("project"),
                    'cost_center':purchase_invoice_doc.cost_center,
                    'account_currency': get_account_currency(wip_account),
                },get_account_currency(wip_account), item=item)
            )
            gl_map.append(
                self.get_gl_dict({
                    'company': self.company,
                    'posting_date': self.posting_date,
                    'fiscal_year': fiscal_year,
                    'voucher_type': 'Purchase Invoice',
                    'voucher_no': purchase_invoice_doc.name,
                    'remarks': self.get("remarks") or self.get("remark"),
                    'account': item.expense_account,
                    'debit': item.base_amount,
                    'credit': 0,
                    'debit_in_account_currency': item.base_amount,
                    'credit_in_account_currency': 0,
                    'is_opening': "No",
                    'party_type': None,
                    'party': None,
                    'project': self.get("project"),
                    'cost_center':purchase_invoice_doc.cost_center,
                    'account_currency': get_account_currency(item.expense_account), 
                },get_account_currency(item.expense_account), item=item)
            )
        if gl_map:
            make_gl_entries(gl_map)
        frappe.db.sql(""" update `tabPurchase Invoice` set work_in_progress=0 where name='{0}'""".format(purchase_invoice['name']))


def before_insert(doc, method):
    if doc.is_new():
        date = datetime.strptime(doc.posting_date, "%Y-%m-%d")
        year = date.strftime('%Y')
        naming_series = doc.get("naming_series")
        if doc.get("is_return"):
            naming_series = "CN-.YYYY.-"
        
        doc.naming_series = naming_series.replace("YYYY",year)
