import frappe
from frappe import _
from erpnext.stock.doctype.batch.batch import set_batch_nos
from erpnext.stock.doctype.delivery_note.delivery_note import DeliveryNote

class CustomDeliveryNote(DeliveryNote):
    def validate(self):
        self.validate_posting_time()
        if not hasattr(self,"from_file"):
            super(DeliveryNote, self).validate()
        self.set_status()
        self.so_required()
        self.validate_proj_cust()
        self.check_sales_order_on_hold_or_close("against_sales_order")
        self.validate_warehouse()
        self.validate_uom_is_integer("stock_uom", "stock_qty")
        self.validate_uom_is_integer("uom", "qty")
        self.validate_with_previous_doc()

        from erpnext.stock.doctype.packed_item.packed_item import make_packing_list

        make_packing_list(self)

        if self._action != "submit" and not self.is_return:
            set_batch_nos(self, "warehouse", throw=True)
            set_batch_nos(self, "warehouse", throw=True, child_table="packed_items")

        self.update_current_stock()

        if not self.installation_status:
            self.installation_status = "Not Installed"
        self.reset_default_field_value("set_warehouse", "items", "warehouse")
